package jcd;

/**
 * These are properties that are to be loaded from workspace_properties.xml. They
 * will provide custom labels and other UI details for this application and
 * it's custom workspace. Note that in this application we're using two different
 * properties XML files. simple_app_properties.xml is for settings known to the
 * Simple App Framework and so helps to set it up. These properties would be for
 * anything relevant to this custom application. The reason for loading this stuff
 * from an XML file like this is to make these settings independent of the code
 * and therefore easily interchangeable, like if we wished to change the language
 * the application ran in.
 * 
 * @author Richard McKenna
 * @author Robert Riselvato
 * @version 1.0
 */
public enum PropertyType {
    SELECTION_TOOL_ICON,
    REMOVE_ICON,
    ADD_CLASS_ICON,
    ADD_INTERFACE_ICON,
    RESIZE_ICON,
    UNDO_ICON,
    REDO_ICON,
    PLUS_ICON,
    MINUS_ICON,
    ZOOM_IN_ICON,
    ZOOM_OUT_ICON,
    SNAPSHOT_ICON,
    
    SELECTION_TOOL_TOOLTIP,
    REMOVE_TOOLTIP,
    ADD_CLASS_TOOLTIP,
    ADD_INTERFACE_TOOLTIP,
    RESIZE_TOOLTIP,
    UNDO_TOOLTIP,
    REDO_TOOLTIP,
    PLUS_TOOLTIP,
    MINUS_TOOLTIP,
    ZOOM_IN_TOOLTIP,
    ZOOM_OUT_TOOLTIP,
    SNAPSHOT_TOOLTIP,
    
    REMOVE_CLASS_MESSAGE,
    REMOVE_INTERFACE_MESSAGE,
    REMOVE_ATTRIBUTE_MESSAGE,
    
    UPDATE_ERROR_MESSAGE,
    UPDATE_ERROR_TITLE,
    
    EXPORT_ICON,
    EXPORT_TOOLTIP
}
