package jcd.gui;

import java.io.IOException;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.BorderPane;
import static jcd.PropertyType.ADD_CLASS_ICON;
import static jcd.PropertyType.ADD_CLASS_TOOLTIP;
import static jcd.PropertyType.ADD_INTERFACE_ICON;
import static jcd.PropertyType.ADD_INTERFACE_TOOLTIP;
import static jcd.PropertyType.MINUS_ICON;
import static jcd.PropertyType.MINUS_TOOLTIP;
import static jcd.PropertyType.PLUS_ICON;
import static jcd.PropertyType.PLUS_TOOLTIP;
import static jcd.PropertyType.REDO_ICON;
import static jcd.PropertyType.REDO_TOOLTIP;
import static jcd.PropertyType.UNDO_ICON;
import static jcd.PropertyType.UNDO_TOOLTIP;
import static jcd.PropertyType.REMOVE_ICON;
import static jcd.PropertyType.REMOVE_TOOLTIP;
import static jcd.PropertyType.RESIZE_ICON;
import static jcd.PropertyType.RESIZE_TOOLTIP;
import static jcd.PropertyType.SELECTION_TOOL_ICON;
import static jcd.PropertyType.SELECTION_TOOL_TOOLTIP;
import static jcd.PropertyType.ZOOM_IN_ICON;
import static jcd.PropertyType.ZOOM_IN_TOOLTIP;
import static jcd.PropertyType.ZOOM_OUT_ICON;
import static jcd.PropertyType.ZOOM_OUT_TOOLTIP;
import static jcd.PropertyType.EXPORT_ICON;
import static jcd.PropertyType.EXPORT_TOOLTIP;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import jcd.data.ClassDesign;
import jcd.controller.WorkAreaController;
import jcd.data.DataManager;
import jcd.data.AttributeDesign;
import jcd.data.LineDesign;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.transform.Scale;
import static jcd.PropertyType.SNAPSHOT_ICON;
import static jcd.PropertyType.SNAPSHOT_TOOLTIP;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author Robert Riselvato
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    // HERE'S THE WORK AREA CONTROLLER
    WorkAreaController workAreaController;
    
    // HERE'S THE DATA MANAGER
    DataManager data;
    
    // THE TOOLBAR ON THE TOP
    HBox toolbarHBox;
    Button selectButton;
    Button resizeButton;
    Button addClassButton;
    Button addInterfaceButton;
    Button removeButton;
    Button undoButton;
    Button redoButton;
    Button zoomInButton;
    Button zoomOutButton;
    Button snapshotButton;
    
    // THE GRID AND SNAP CONTROLS
    VBox gridAndSnapVBox;
    CheckBox grid;
    CheckBox snap;
    
    // THE CLASS AND INTERFACE DESCRIPTION AND EDITING PANE
    VBox classAndInterfaceVBox;
    
    // THE CLASS AND INTERFACE NAME CONTROLS
    HBox classAndInterfaceNameHBox;
    Label classAndInterfaceNameLabel;
    TextField classAndInterfaceNameTextField;
    
    // THE PACKAGE CONTROLS
    HBox packageHBox;
    Label packageLabel;
    TextField packageTextField;
    
    // THE PARENT CONTROLS
    HBox parentHBox;
    Label parentLabel;
    ComboBox<HBox> parentComboBox;
    
    // THE VARIABLES CONTROLS
    HBox variablesHBox;
    Label variablesLabel;
    Button addVariableButton;
    Button removeVariableButton;
    GridPane variablesGridPane;
    ScrollPane variablesScrollPane;
    Label variableName;
    Label variableType;
    Label variableStatic;
    Label variableAccess;
    
    // THE METHODS CONTROLS
    HBox methodsHBox;
    Label methodsLabel;
    Button addMethodButton;
    Button removeMethodButton;
    GridPane methodsGridPane;
    ScrollPane methodsScrollPane;
    Label methodName;
    Label methodReturn;
    Label methodStatic;
    Label methodAbstract;
    Label methodAccess;
    Label methodArg1;
    Label methodArg2;
    
    // THE WORK AREA FOR THE UML DIAGRAM
    Pane workArea;
    ScrollPane workAreaScrollPane;
    Pane gridPane;
    
    // ARRAY LIST FOR BUTTON
    ArrayList<Button> buttons = new ArrayList<Button>();
    
    // THE SELECTED DESIGN
    ClassDesign selectedDesign = new ClassDesign();
    AttributeDesign selectedVariable = new AttributeDesign(false);
    AttributeDesign selectedMethod = new AttributeDesign(true);
    
    // THE EXPORT BUTTON
    Button exportButton;
    
    // IF A LINE IS SELECTED
    boolean selectedLine = false;
    LineDesign lineDesign;
    
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI(); 
        
        data = (DataManager) app.getDataComponent();
        
        // THE WORKSPACE WILL BE CREATED WITH A BORDER PANE
        workspace = new BorderPane();
        
        // THE BUTTONS FOR THE TOP TOOLBAR CONTROLS
        toolbarHBox = new HBox();
        snapshotButton = gui.initChildButton(toolbarHBox, SNAPSHOT_ICON.toString(), SNAPSHOT_TOOLTIP.toString(), false);
        snapshotButton.setPrefWidth(90);
        selectButton = gui.initChildButton(toolbarHBox, SELECTION_TOOL_ICON.toString(), SELECTION_TOOL_TOOLTIP.toString(), false);
        selectButton.setPrefWidth(90); buttons.add(selectButton);
        resizeButton = gui.initChildButton(toolbarHBox, RESIZE_ICON.toString(), RESIZE_TOOLTIP.toString(), false);
        resizeButton.setPrefWidth(90); buttons.add(resizeButton);
        addClassButton = gui.initChildButton(toolbarHBox, ADD_CLASS_ICON.toString(), ADD_CLASS_TOOLTIP.toString(), false);
        addClassButton.setPrefWidth(90); buttons.add(addClassButton);
        addInterfaceButton = gui.initChildButton(toolbarHBox, ADD_INTERFACE_ICON.toString(), ADD_INTERFACE_TOOLTIP.toString(), false);
        addInterfaceButton.setPrefWidth(90); buttons.add(addInterfaceButton);
        removeButton = gui.initChildButton(toolbarHBox, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), false);
        removeButton.setPrefWidth(90);
        undoButton = gui.initChildButton(toolbarHBox, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), false);
        undoButton.setPrefWidth(90);
        redoButton = gui.initChildButton(toolbarHBox, REDO_ICON.toString(), REDO_TOOLTIP.toString(), false);
        redoButton.setPrefWidth(90);
        zoomInButton = gui.initChildButton(toolbarHBox, ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), false);
        zoomInButton.setPrefWidth(90);
        zoomOutButton = gui.initChildButton(toolbarHBox, ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), false);
        zoomOutButton.setPrefWidth(90);
        
        // THE PANE FOR THE GRID AND SNAP CHECK BOXES IN THE UPPER TOOLBAR
        gridAndSnapVBox = new VBox();
        grid = new CheckBox("Grid");
        snap = new CheckBox("Snap");
        gridAndSnapVBox.getChildren().addAll(grid, snap);
        toolbarHBox.getChildren().add(gridAndSnapVBox);
        gridAndSnapVBox.setSpacing(5);
        
        // THE CONTROLS FOR THE CLASS AND INTERFACE DESCRIPTION AND EDITING PANE
        classAndInterfaceVBox = new VBox();
        
        // THE LABEL AND TEXT FIELD FOR THE CLASS AND INTERFACE NAME
        classAndInterfaceNameHBox = new HBox();
        classAndInterfaceNameLabel = new Label("Class/Interface Name: ");
        classAndInterfaceNameTextField = new TextField();
        classAndInterfaceNameHBox = new HBox(classAndInterfaceNameLabel, classAndInterfaceNameTextField);
        classAndInterfaceNameHBox.setSpacing(20);
        
        // THE LABEL AND TEXT FIELD FOR THE PACKAGE
        packageHBox = new HBox();
        packageLabel = new Label("Package: ");
        packageTextField = new TextField();
        packageHBox = new HBox(packageLabel, packageTextField);
        packageHBox.setSpacing(105);
        
        // THE LABEL AND CHOICE BOX FOR THE PARENT
        parentHBox = new HBox();
        parentLabel = new Label("Parent: ");
        parentComboBox = new ComboBox<HBox>();
        parentComboBox.setEditable(true);
        parentHBox = new HBox(parentLabel, parentComboBox);
        parentHBox.setSpacing(115);
        
        // EMPTY PANE FOR SPACING PURPOSES
        Pane emptyPane = new Pane();
        emptyPane.setPrefHeight(25);
        
        // THE LABELS AND BUTTONS FOR THE VARIABLES
        variablesHBox = new HBox();
        variablesLabel = new Label("\nVariables: ");
        variablesHBox.getChildren().add(variablesLabel);
        addVariableButton = gui.initChildButton(variablesHBox, PLUS_ICON.toString(), PLUS_TOOLTIP.toString(), false);
        removeVariableButton = gui.initChildButton(variablesHBox, MINUS_ICON.toString(), MINUS_TOOLTIP.toString(), false);
        variablesHBox.setSpacing(10);
        
        // THE PANE FOR THE VARIABLES
        variablesGridPane = new GridPane();
        variablesScrollPane = new ScrollPane(variablesGridPane);
        variablesScrollPane.setPrefViewportHeight(100);
        variableName = new Label("\nName\t");
        variableType = new Label("\nType\t");
        variableStatic = new Label("\nStatic\t");
        variableAccess = new Label("\nAccess\t");
        variablesGridPane.addRow(0, variableName, variableType, variableStatic, variableAccess);
        variablesGridPane.setGridLinesVisible(true);
        variablesScrollPane.setPrefViewportWidth(300);
        variablesGridPane.setHgap(5);
        variablesGridPane.setVgap(5);
        
        // THE LABELS AND BUTTONS FOR THE METHODS
        methodsHBox = new HBox();
        methodsLabel = new Label("\nMethods: ");
        methodsHBox.getChildren().add(methodsLabel);
        addMethodButton = gui.initChildButton(methodsHBox, PLUS_ICON.toString(), PLUS_TOOLTIP.toString(), false);
        removeMethodButton = gui.initChildButton(methodsHBox, MINUS_ICON.toString(), MINUS_TOOLTIP.toString(), false);
        methodsHBox.setSpacing(10);
        
        // THE PANE FOR THE METHODS
        methodsGridPane = new GridPane();
        methodsScrollPane = new ScrollPane(methodsGridPane);
        methodsScrollPane.setPrefViewportHeight(100);
        methodName = new Label("\nName\t");
        methodReturn = new Label("\nReturn\t");
        methodStatic = new Label("\nStatic\t");
        methodAbstract = new Label("\nAbstract\t\t");
        methodAccess = new Label("\nAccess\t");
        methodArg1 = new Label("\nArg1\t");
        methodArg2 = new Label("\nArg2\t");
        methodsGridPane.addRow(0, methodName, methodReturn, methodStatic, methodAbstract, methodAccess, methodArg1, methodArg2);
        methodsGridPane.setGridLinesVisible(true);
        methodsScrollPane.setPrefViewportWidth(300);
        methodsGridPane.setHgap(5);
        
        // NOW ADD THE CONTROLS TO THE CLASS AND INTERFACE DESCRIPTION AND EDITING PANE
        classAndInterfaceVBox = new VBox(classAndInterfaceNameHBox, packageHBox, parentHBox, emptyPane, variablesHBox, variablesScrollPane, methodsHBox, methodsScrollPane);
        
        // THE PANE FOR THE WORK AREA
        gridPane = new Pane();
        gridPane.setMinWidth(4000);
        gridPane.setMinHeight(2000);
        gridPane.setVisible(false);
        workArea = new Pane();
        workArea.setPrefSize(4000, 2000);
        setTransformations();
        workArea.getChildren().add(gridPane);
        workAreaScrollPane = new ScrollPane(workArea);
        
        // NOW ADD THE TOOLBAR, CLASS & INTERFACE DESCRIPTION PANE, AND WORK AREA PANE TO THE WORSPACE
        ((BorderPane) workspace).setTop(toolbarHBox);
        ((BorderPane) workspace).setRight(classAndInterfaceVBox);
        ((BorderPane) workspace).setCenter(workAreaScrollPane);
        
        exportButton = gui.initChildButton((Pane) gui.getAppPane().getTop(), EXPORT_ICON.toString(), EXPORT_TOOLTIP.toString(), false);
        
        disableFunctions();
        removeButton.setDisable(true);
        undoButton.setDisable(true);
        redoButton.setDisable(true);
        
        initHandlers();
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamically as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        toolbarHBox.getStyleClass().add("toolbar_pane");
        classAndInterfaceVBox.getStyleClass().add(CLASS_BORDERED_PANE);
        for (ClassDesign design : data.getDesigns()) {
            design.getStyleClass().clear();
            design.getStyleClass().add("design_pane");
        }
        selectedDesign.getStyleClass().clear();
        selectedDesign.getStyleClass().add("selected_design_pane");
        if (!selectedLine) {
            for (LineDesign line : data.getLines()) {
                workArea.getChildren().remove(line.getLine());
                line.getLine().setStroke(Color.BLACK);
                workArea.getChildren().add(line.getLine());
                line.getLine().toBack();
            }
        }
        gridPane.getStyleClass().add("grid_pane");
        gridPane.toBack();
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        ArrayList<ClassDesign> designs = data.getDesigns();
        ArrayList<LineDesign> lines = data.getLines();
        workArea.getChildren().clear();
        for (ClassDesign design : designs) {
            workArea.getChildren().add(design);
            design.relocate(design.getXPosition(), design.getYPosition());
            design.setOnMousePressed(e -> {
                if (selectButton.isDisabled() || resizeButton.isDisabled()) {
                    selectedDesign = design;
                    selectedLine = false;
                    initStyle();
                    classAndInterfaceNameTextField.setText(selectedDesign.getName());
                    packageTextField.setText(selectedDesign.getPackage());
                    enableFunctions(); removeMethodButton.setDisable(true); removeVariableButton.setDisable(true);
                    updateParentMenu(design); parentComboBox.getEditor().setText(""); parentComboBox.setValue(null);
                    removeButton.setDisable(false);
                }
            });
            
            /*design.setOnMouseReleased(e -> {
                workAreaController.enqueueUndo();
                undoButton.setDisable(false);
            });*/
            
            design.getNameLabel().textProperty().addListener(e -> {
                workAreaController.enqueueUndo();
                undoButton.setDisable(false);
                redoButton.setDisable(true);
            });
            
            design.getPackageLabel().textProperty().addListener(e -> {
                workAreaController.enqueueUndo();
                undoButton.setDisable(false);
                redoButton.setDisable(true);
            });
        }
        for (LineDesign line : lines) {
            workArea.getChildren().add(line.getLine());
            line.getLine().toBack();
            workArea.getChildren().add(line.getArrow());
            initLineHandler(line);
        }
        if (workAreaController.getUndoSize() == 1 && workAreaController.getRedoSize() == 0 && data.getDesigns().size() > 1) {
            workAreaController.resetUndo();
        }
        workArea.getChildren().add(gridPane);
        gridPane.toBack();
    }
    
    /**
     * Helper method for initiating the event handlers for the buttons
     */
    private void initHandlers() {
        workAreaController = new WorkAreaController(app);
        
        for (Button button : buttons) {
            button.setOnAction(e -> {
                for (Button button1 : buttons) {
                    button1.setDisable(false);
                }
                button.setDisable(true);
                selectedLine = false;
                disableFunctions();
                removeButton.setDisable(true);
            });
        }
        
        workArea.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (addClassButton.isDisabled() && !snap.isSelected()) {
                    workAreaController.handleAddClass(event.getX(), event.getY());
                    reloadWorkspace();
                } else if (addInterfaceButton.isDisabled() && !snap.isSelected()) {
                    workAreaController.handleAddInterface(event.getX(), event.getY());
                    reloadWorkspace();
                } else if (addClassButton.isDisabled() && snap.isSelected()) {
                    workAreaController.handleSnapAddClass(event.getX(), event.getY());
                    reloadWorkspace();
                } else if (addInterfaceButton.isDisabled() && snap.isSelected()) {
                    workAreaController.handleSnapAddInterface(event.getX(), event.getY());
                    reloadWorkspace();
                }
            }
        });
        
        workArea.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (selectedDesign.getXPosition() <= event.getX() && selectedDesign.getXPosition() + selectedDesign.prefWidth(-1) >= event.getX()
                        && selectedDesign.getYPosition() <= event.getY() && selectedDesign.getYPosition() + selectedDesign.prefHeight(-1) >= event.getY()
                        && selectButton.isDisabled()) {
                    if (!snap.isSelected()) {
                        double x = selectedDesign.getCenter()[0];
                        double y = selectedDesign.getCenter()[1];
                        selectedDesign.setXPosition(event.getX() - selectedDesign.prefWidth(-1) / 2);
                        selectedDesign.setYPosition(event.getY() - selectedDesign.prefHeight(-1) / 2);
                        selectedDesign.relocate(selectedDesign.getXPosition(), selectedDesign.getYPosition());
                    } else {
                        workAreaController.handleSnapMove(selectedDesign, event.getX(), event.getY());
                    }
                    updateLinePositions(event.getX(), event.getY());
                } else if (selectedLine && selectButton.isDisabled()) {
                    workArea.getChildren().remove(lineDesign.getLine());
                    double[] values = {event.getX(), event.getY()};
                    int[] points = lineDesign.getClosestPoint(event.getX(), event.getY());
                    if (points[0] == 8 && points[1] == 9) {
                        workArea.getChildren().remove(lineDesign.getArrow());
                    }
                    lineDesign.setPoint(points, values);
                    lineDesign.getLine().setStroke(Color.YELLOW);
                    workArea.getChildren().add(lineDesign.getLine());
                    lineDesign.getLine().toBack();
                    if (points[0] == 8 && points[1] == 9) {
                        workArea.getChildren().add(lineDesign.getArrow());
                    }
                    initLineHandler(lineDesign);
                    gridPane.toBack();
                } else if (resizeButton.isDisabled()) {
                    if (!snap.isSelected()) {
                        selectedDesign.setMinWidth(Math.max(selectedDesign.prefWidth(-1), -selectedDesign.getXPosition() + event.getX()));
                        selectedDesign.setMinHeight(Math.max(selectedDesign.prefHeight(-1), -selectedDesign.getYPosition() + event.getY()));
                    } else {
                        workAreaController.handleSnapResize(selectedDesign, event.getX(), event.getY());
                    }
                    updateLinePositions(selectedDesign.getCenter()[0], selectedDesign.getCenter()[1]);
                }
            }
        });
        
        classAndInterfaceNameTextField.textProperty().addListener(e -> {
            selectedDesign.setName(classAndInterfaceNameTextField.getText());
            if (workAreaController.handleClassAndPackageName(selectedDesign)) {
                reloadWorkspace();
                classAndInterfaceNameTextField.setText(selectedDesign.getName());
                packageTextField.setText(selectedDesign.getPackage());
            }
        });
        
        packageTextField.textProperty().addListener(e -> {
            selectedDesign.setPackage(packageTextField.getText());
            if (workAreaController.handleClassAndPackageName(selectedDesign)) {
                reloadWorkspace();
                classAndInterfaceNameTextField.setText(selectedDesign.getName());
                packageTextField.setText(selectedDesign.getPackage());
            }
        });
        
        exportButton.setOnAction(e -> {
            workAreaController.handleExportRequest();
        });
        
        addVariableButton.setOnAction(e -> {
            workAreaController.handleAddVariable(selectedDesign);
            reloadAttributeToolbar();
            workAreaController.enqueueUndo();
            undoButton.setDisable(false);
            redoButton.setDisable(true);
        });
        
        removeVariableButton.setOnAction(e -> {
            workAreaController.handleRemoveVariable(selectedDesign, selectedVariable);
            reloadAttributeToolbar();
            removeVariableButton.setDisable(true);
            updateAggregateLines(selectedDesign); updateAssociateLines(selectedDesign);
            workAreaController.enqueueUndo();
            undoButton.setDisable(false);
            redoButton.setDisable(true);
        });
        
        addMethodButton.setOnAction(e -> {
            workAreaController.handleAddMethod(selectedDesign);
            reloadAttributeToolbar();
            workAreaController.enqueueUndo();
            undoButton.setDisable(false);
            redoButton.setDisable(true);
        });
        
        removeMethodButton.setOnAction(e -> {
            workAreaController.handleRemoveMethod(selectedDesign, selectedMethod);
            reloadAttributeToolbar();
            removeMethodButton.setDisable(true);
            updateAssociateLines(selectedDesign);
            workAreaController.enqueueUndo();
            undoButton.setDisable(false);
            redoButton.setDisable(true);
        });
        
        toolbarHBox.setOnMouseClicked(e -> {
            disableFunctions();
            removeButton.setDisable(true);
        });
        
        removeButton.setOnAction(e -> {
            data.getDesigns().remove(selectedDesign);
            ArrayList<LineDesign> nullLines = new ArrayList<LineDesign>();
            for (LineDesign line : data.getLines()) {
                if (selectedDesign.equals(line.getStartClass()) || selectedDesign.equals(line.getEndClass())) {
                    nullLines.add(line);
                }
            }
            for (LineDesign line : nullLines) {
                data.getLines().remove(line);
            }
            selectedDesign = new ClassDesign();
            reloadWorkspace();
            workAreaController.enqueueUndo();
            disableFunctions();
            removeButton.setDisable(true);
            undoButton.setDisable(false);
            redoButton.setDisable(true);
        });
        
        workArea.setOnMouseReleased(e -> { 
            workAreaController.enqueueUndo(); 
            undoButton.setDisable(false);
            redoButton.setDisable(true);
        });
        
        undoButton.setOnAction(e -> {
            workAreaController.handleUndo();
            redoButton.setDisable(false);
            reloadWorkspace();
            if (workAreaController.getUndoSize() == 1) {
                undoButton.setDisable(true);
            }
            disableFunctions();
        });
        
        redoButton.setOnAction(e -> {
            workAreaController.handleRedo();
            undoButton.setDisable(false);
            reloadWorkspace();
            if (workAreaController.getRedoSize() == 0) {
                redoButton.setDisable(true);
            }
            disableFunctions();
        });
        
        zoomInButton.setOnAction(e -> {
            if (workAreaController.handleZoomIn(workArea)) {
                zoomInButton.setDisable(true);
            }
            zoomOutButton.setDisable(false);
        });
        
        zoomOutButton.setOnAction(e -> {
            if (workAreaController.handleZoomOut(workArea)) {
                zoomOutButton.setDisable(true);
            }
            zoomInButton.setDisable(false);
        });
        
        grid.selectedProperty().addListener(e -> {
            gridPane.setVisible(grid.isSelected());
        });
        
        snapshotButton.setOnAction(e -> {
            workAreaController.handleSnapshot(workArea);
        });
    }
    
    /**
     * Initiates the event handlers of the given line
     * @param line 
     *      the line whose event handler is to be initiated
     */
    public void initLineHandler(LineDesign line) {
        line.getLine().setOnMousePressed(e -> {
            selectedLine = false;
            initStyle();
            selectedLine = true;
            lineDesign = line;
            workArea.getChildren().remove(line.getLine());
            line.getLine().setStroke(Color.YELLOW);
            workArea.getChildren().add(line.getLine());
            line.getLine().toBack();
            selectedDesign = new ClassDesign();
            initStyle();
            classAndInterfaceNameTextField.setText(selectedDesign.getName());
            packageTextField.setText(selectedDesign.getPackage());
            variablesGridPane.getChildren().clear();
            variablesGridPane.addRow(0, variableName, variableType, variableStatic, variableAccess);
            methodsGridPane.getChildren().clear();
            methodsGridPane.addRow(0, methodName, methodReturn, methodStatic, methodAbstract, methodAccess, methodArg1, methodArg2);
            disableFunctions();
            removeButton.setDisable(true);
            gridPane.toBack();
        });
    }
    
    /**
     * Helper method for reloading the variables and methods of the selected design
     */
    public void reloadAttributeToolbar() {
        variablesGridPane.getChildren().clear();
        variablesGridPane.addRow(0, variableName, variableType, variableStatic, variableAccess);
        for (int i = 0; i < selectedDesign.getVariables().size(); i++) {
            AttributeDesign variable = selectedDesign.getVariables().get(i);
            variablesGridPane.addRow(i + 1, variable.getNameTextField(), variable.getTypeTextField(), variable.getStaticCheckBox(), variable.getAccessChoiceBox());
            initVariableHandler(variable);
            variable.disableAll();
        }
        
        methodsGridPane.getChildren().clear();
        methodsGridPane.addRow(0, methodName, methodReturn, methodStatic, methodAbstract, methodAccess, methodArg1, methodArg2);
        for (int i = 0; i < selectedDesign.getMethods().size(); i++) {
            AttributeDesign method = selectedDesign.getMethods().get(i);
            Node[] nodes = new Node[6 + method.getParemeters().size()];
            nodes[0] = method.getNameTextField(); nodes[1] = method.getTypeTextField(); nodes[2] = method.getStaticCheckBox(); 
            nodes[3] = method.getAbstractCheckBox(); nodes[4] = method.getAccessChoiceBox();
            for (int j = 0; j < method.getParemeters().size(); j++) {
                nodes[5 + j] = method.getParemeters().get(j);
            }
            nodes[5 + method.getParemeters().size()] = method.getButtons();
            methodsGridPane.addRow(i + 1, nodes);
            initMethodHandler(method);
            method.disableAll();
        }
    }
    
    /**
     * Initiates the event handlers for a variable of the selected design
     * @param variable 
     *      the variable whose handlers are to be initiated
     */
    public void initVariableHandler(AttributeDesign variable) {
        variable.getNameTextField().setOnMousePressed(e -> {
            for (AttributeDesign v : selectedDesign.getVariables()) {
                v.disableAll();
            }
            variable.enableAll();
            selectedVariable = variable; removeVariableButton.setDisable(false);
        });
        variable.getNameTextField().textProperty().addListener(e -> {
            selectedDesign.reloadVariablesVBox();
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
        variable.getTypeTextField().setOnMousePressed(e -> {
            for (AttributeDesign v : selectedDesign.getVariables()) {
                v.disableAll();
            }
            variable.enableAll();
            selectedVariable = variable; removeVariableButton.setDisable(false);
        });
        variable.getTypeTextField().textProperty().addListener(e -> {
            selectedDesign.reloadVariablesVBox();
            updateAggregateLines(selectedDesign); updateAssociateLines(selectedDesign);
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
        variable.getStaticCheckBox().setOnMousePressed(e -> {
            for (AttributeDesign v : selectedDesign.getVariables()) {
                v.disableAll();
            }
            variable.enableAll();
            selectedVariable = variable; removeVariableButton.setDisable(false);
        });
        variable.getStaticCheckBox().selectedProperty().addListener(e -> {
            variable.setIsStatic(variable.getStaticCheckBox().isSelected()); selectedDesign.reloadVariablesVBox();
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
        variable.getAccessChoiceBox().setOnMousePressed(e -> {
            for (AttributeDesign v : selectedDesign.getVariables()) {
                v.disableAll();
            }
            variable.enableAll();
            selectedVariable = variable; removeVariableButton.setDisable(false);
        });
        variable.getAccessChoiceBox().valueProperty().addListener(e -> {
            selectedDesign.reloadVariablesVBox();
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
    }
    
    /**
     * Initiates the event handlers for a method of the selected design
     * @param method 
     *      the method whose handlers are to be initiated
     */
    public void initMethodHandler(AttributeDesign method) {
        method.getNameTextField().setOnMousePressed(e -> {
            for (AttributeDesign m : selectedDesign.getMethods()) {
                m.disableAll();
            }
            method.enableAll();
            selectedMethod = method; removeMethodButton.setDisable(false);
        });
        method.getNameTextField().textProperty().addListener(e -> {
            selectedDesign.reloadMethodsVBox();
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
        method.getTypeTextField().setOnMousePressed(e -> {
            for (AttributeDesign m : selectedDesign.getMethods()) {
                m.disableAll();
            }
            method.enableAll();
            selectedMethod = method; removeMethodButton.setDisable(false);
        });
        method.getTypeTextField().textProperty().addListener(e -> {
            selectedDesign.reloadMethodsVBox();
            updateAssociateLines(selectedDesign);
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
        method.getStaticCheckBox().setOnMousePressed(e -> {
            for (AttributeDesign m : selectedDesign.getMethods()) {
                m.disableAll();
            }
            method.enableAll();
            selectedMethod = method; removeMethodButton.setDisable(false);
        });
        method.getStaticCheckBox().selectedProperty().addListener(e -> {
            method.setIsStatic(method.getStaticCheckBox().isSelected()); selectedDesign.reloadMethodsVBox();
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
        method.getAccessChoiceBox().setOnMousePressed(e -> {
            for (AttributeDesign m : selectedDesign.getMethods()) {
                m.disableAll();
            }
            method.enableAll();
            selectedMethod = method; removeMethodButton.setDisable(false);
        });
        method.getAccessChoiceBox().valueProperty().addListener(e -> {
            selectedDesign.reloadMethodsVBox();
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
        method.getAbstractCheckBox().setOnMousePressed(e -> {
            for (AttributeDesign m : selectedDesign.getMethods()) {
                m.disableAll();
            }
            method.enableAll();
            selectedMethod = method; removeMethodButton.setDisable(false);
        });
        method.getAbstractCheckBox().selectedProperty().addListener(e -> {
            method.setIsAbstract(method.getAbstractCheckBox().isSelected()); selectedDesign.reloadMethodsVBox();
            selectedDesign.updateIsAbstract();
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
        for (TextField param : method.getParemeters()) {
            param.setOnMousePressed(e -> {
                for (AttributeDesign m : selectedDesign.getMethods()) {
                    m.disableAll();
                }
                method.enableAll();
                selectedMethod = method; removeMethodButton.setDisable(false);
            });
            param.textProperty().addListener(e -> {
                selectedDesign.reloadMethodsVBox();
                updateAssociateLines(selectedDesign);
                workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
            });
        }
        method.getAddParamButton().setOnAction(e -> {
            method.addParameter(new TextField(""));
            reloadAttributeToolbar();
            selectedDesign.reloadMethodsVBox();
            for (AttributeDesign m : selectedDesign.getMethods()) {
                m.disableAll();
            }
            method.enableAll();
            selectedMethod = method; removeMethodButton.setDisable(false);
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
        method.getRemoveParamButton().setOnAction(e -> {
            method.removeParameter();
            reloadAttributeToolbar();
            selectedDesign.reloadMethodsVBox();
            for (AttributeDesign m : selectedDesign.getMethods()) {
                m.disableAll();
            }
            method.enableAll();
            selectedMethod = method; removeMethodButton.setDisable(false);
            updateAssociateLines(selectedDesign);
            workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        });
    }
    
    public void disableFunctions() {
        selectedDesign = new ClassDesign();
        reloadAttributeToolbar();
        classAndInterfaceNameTextField.setDisable(true);
        classAndInterfaceNameTextField.setText("");
        packageTextField.setDisable(true);
        packageTextField.setText("");
        addVariableButton.setDisable(true); 
        removeVariableButton.setDisable(true);
        addMethodButton.setDisable(true);
        removeMethodButton.setDisable(true);
        parentComboBox.getEditor().setText("");
        parentComboBox.setDisable(true);
        parentComboBox.setValue(null);
        initStyle();
    }
    
    public void enableFunctions() {
        reloadAttributeToolbar();
        classAndInterfaceNameTextField.setDisable(false);
        packageTextField.setDisable(false);
        addVariableButton.setDisable(false);
        addMethodButton.setDisable(false);
        parentComboBox.setDisable(false);
    }
    
    public void updateParentMenu(ClassDesign design) {
        parentComboBox.getItems().clear();
        ArrayList<HBox> hboxes = new ArrayList<HBox>();
        for (ClassDesign d : data.getDesigns()) {
            if (!design.equals(d)) {
                CheckBox cb = new CheckBox();
                Label l = new Label(d.getName());
                if (design.getParents().contains(d)) {
                    cb.setSelected(true);
                } else {
                    cb.setSelected(false);
                }
                hboxes.add(new HBox(cb, l));
                initParentControls(cb, d);
            }
        }
        parentComboBox.getItems().addAll(hboxes);
        parentComboBox.setOnHidden(e -> {
            parentComboBox.getEditor().setText("");
            if (parentComboBox.getValue() != null) {
                CheckBox temp = (CheckBox) parentComboBox.getValue().getChildren().get(0);
                temp.setSelected(!temp.isSelected());
            }
        });
        parentComboBox.getEditor().textProperty().addListener(e -> {
            String text = parentComboBox.getEditor().getText();
            if (!text.contains("@") && !text.equals("") && !text.contains("!")) {
                if (text.length() == 1 && data.getDesign(text) == null) {
                    ClassDesign newDesign = new ClassDesign(selectedDesign.getXPosition(), Math.max(selectedDesign.getYPosition() - 200, 0), false);
                    newDesign.setName(text);
                    data.addDesign(newDesign);
                    selectedDesign.addParent(newDesign);
                    updateLines(selectedDesign, newDesign, true);
                } else if (text.length() > 1) {
                    if (data.getDesign(text.substring(0, text.length() - 1)) != null) {
                        ClassDesign parent = data.getDesign(text.substring(0, text.length() - 1));
                        parent.setName(text);
                        if (workAreaController.handleClassAndPackageName(parent)) {
                            parentComboBox.getEditor().setText("[!]" + text);
                        }
                    } else if (data.getDesignSubstring(text) != null) {
                        ClassDesign parent = data.getDesignSubstring(text);
                        parent.setName(text);
                        if (workAreaController.handleClassAndPackageName(parent)) {
                            parentComboBox.getEditor().setText("[!]" + text);
                        }
                    }
                }
            }
        });
        parentComboBox.setOnShown(e -> {
            if (!parentComboBox.getEditor().getText().equals("")) {
                String text = parentComboBox.getEditor().getText();
                parentComboBox.getEditor().setText("");
                if (data.getDesign(text) != null) {
                    ClassDesign parent = data.getDesign(text);
                    CheckBox cb = new CheckBox(); cb.setSelected(true);
                    HBox hb = new HBox(cb, new Label(text));
                    parentComboBox.getItems().add(hb);
                    parentComboBox.setValue(null);
                    initParentControls(cb, parent);
                }
            }
        });
    }
    
    public void initParentControls(CheckBox cb, ClassDesign parent) {
        cb.selectedProperty().addListener(e -> {
            if (cb.isSelected()) {
                selectedDesign.addParent(parent);
                updateLines(selectedDesign, parent, true);
            } else {
                selectedDesign.getParents().remove(parent);
                updateLines(selectedDesign, parent, false);
            }
        });
    }
    
    public void updateLines(ClassDesign design, ClassDesign parent, boolean add) {
        if (add) {
            data.addLine(new LineDesign(design, parent, "extends"));
        } else {
            LineDesign line = data.getLines().get(0);
            int i = 0;
            while (!(line.getStartClass().equals(design) && line.getEndClass().equals(parent) && line.getRelation().equals("extends"))) {
                i++;
                line = data.getLines().get(i);
            }
            data.getLines().remove(line);
        }
        updateAggregateLines(design); updateAssociateLines(design);
        workAreaController.enqueueUndo(); undoButton.setDisable(false); redoButton.setDisable(true);
        reloadWorkspace();
    }
    
    public void updateLinePositions(double eventX, double eventY) {
        double x = selectedDesign.getCenter()[0];
        double y = selectedDesign.getCenter()[1];
        for (LineDesign line : data.getLines()) {
            if (line.getStartClass() == selectedDesign) {
                workArea.getChildren().remove(line.getLine());
                workArea.getChildren().remove(line.getArrow());
                int[] points = {0, 1};
                double[] values = {eventX, eventY};
                line.setPoint(points, values);
                workArea.getChildren().add(line.getLine());
                line.getLine().toBack();
                workArea.getChildren().add(line.getArrow());
                initLineHandler(line);
            } else if (line.getEndClass() == selectedDesign) {
                workArea.getChildren().remove(line.getLine());
                workArea.getChildren().remove(line.getArrow());
                int[] points = {8, 9};
                double[] values = {eventX - x + line.getPoints()[8], eventY - y + line.getPoints()[9]};
                line.setPoint(points, values);
                workArea.getChildren().add(line.getLine());
                line.getLine().toBack();
                workArea.getChildren().add(line.getArrow());
                initLineHandler(line);
            }
        }
        gridPane.toBack();
    }
    
    public void setTransformations() {
        workArea.getTransforms().add(new Scale(0.3, 0.3));
        workArea.getTransforms().add(new Scale(0.4 / 0.3, 0.4 / 0.3));
        workArea.getTransforms().add(new Scale(0.5 / 0.4, 0.5 / 0.4));
        workArea.getTransforms().add(new Scale(0.6 / 0.5, 0.6 / 0.5));
        workArea.getTransforms().add(new Scale(0.7 / 0.6, 0.7 / 0.6));
        workArea.getTransforms().add(new Scale(0.8 / 0.7, 0.8 / 0.7));
        workArea.getTransforms().add(new Scale(0.9 / 0.8, 0.9 / 0.8));
        workArea.getTransforms().add(new Scale(1 / 0.9, 1 / 0.9));
    }
    
    public void updateAggregateLines(ClassDesign design) {
        data.getLines().removeAll(design.removeAggregates());
        data.getLines().addAll(design.addAggregates(data.getDesigns()));
        reloadWorkspace();
    }
    
    public void updateAssociateLines(ClassDesign design) {
        data.getLines().removeAll(design.removeAssociates());
        data.getLines().addAll(design.addAssociates(data.getDesigns()));
        reloadWorkspace();
    }
}
