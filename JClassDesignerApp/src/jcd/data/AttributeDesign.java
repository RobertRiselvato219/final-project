package jcd.data;

import javafx.scene.control.TextField;
import javafx.scene.control.ChoiceBox;
import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;

/**
 * @author Rob
 */
public class AttributeDesign {
    
    // DETERMINES IF THIS DESIGN IS A METHOD
    private boolean isMethod; 
    
    // THE NAME OF THIS DESIGN
    private TextField name = new TextField("");
    
    // THE RETURN TYPE OF THIS DESIGN
    private TextField type = new TextField("");
    
    // DETERMINES IF THIS DESIGN IS STATIC
    private boolean isStatic = false;
    private CheckBox staticCheckBox;
    
    // DETERMINES THE ACCESSIBILITY OF THIS DESIGN
    private ChoiceBox<String> access = new ChoiceBox<String>();
    
    // DETERMINES IF THIS ATTRIBUTE IS ABSTRACT, GIVEN THAT THIS ATTRIBUTE IS A METHOD
    private boolean isAbstract = false;
    private CheckBox abstractCheckBox;
    
    // THE PARAMETERS OF THIS ATTRIBUTE, GIVEN THAT THIS ATTRIBUTE IS A METHOD
    private ArrayList<TextField> parameters = new ArrayList<TextField>();
    private HBox buttons;
    private Button addParamButton;
    private Button removeParamButton;
    
    /**
     * Creates a new Attribute
     */
    public AttributeDesign() {
        access.getItems().addAll("public", "private", "protected");
        staticCheckBox = new CheckBox();
        abstractCheckBox = new CheckBox();
        addParamButton = new Button("+");
        removeParamButton = new Button("-");
        buttons = new HBox(addParamButton, removeParamButton);
        removeParamButton.setDisable(true);
    }
    
    /**
     * Creates a new Attribute for this design with the given parameter
     * Overloaded constructor
     * @param isMethod 
     *      determines if this attribute is a method or not
     */
    public AttributeDesign(boolean isMethod) {
        this();
        this.isMethod = isMethod;
        access.setValue("public");
    }
    
    /**
     * Getter method for the status of this Attribute
     * @return 
     *      true if this Attribute is a method, false otherwise
     */
    public boolean isMethod() {
        return isMethod;
    }
    
    /**
     * Getter method for the name of this Attribute
     * @return 
     *      the name of this Attribute as a String
     */
    public String getName() {
        return name.getText();
    }
    
    /**
     * Getter method for the return type of this Attribute
     * @return 
     *      the return type of this Attribute as a String
     */
    public String getType() {
        return type.getText();
    }
    
    /**
     * Getter method for whether this Attribute is static or not
     * @return 
     *      true if this Attribute is static, false otherwise
     */
    public boolean isStatic() {
        return isStatic;
    }
    
    /**
     * Getter method for the accessibility of this Attribute
     * @return 
     *      the accessibility of this Attribute as a String
     */
    public String getAccess() {
        return access.getValue();
    }
    
    /**
     * Getter method for the abstractness of this Attribute
     * @return 
     *      true if this Attribute is an abstract method, false otherwise
     */
    public boolean isAbstract() {
        return isAbstract;
    }
    
    /**
     * Getter method for the parameters of this Attribute,
     * if this Attribute is a method
     * @return 
     *      the parameters as an ArrayList of TextField
     */
    public ArrayList<TextField> getParemeters() {
        return parameters;
    }
    
    /**
     * Setter method for the name of this attribute
     * @param name 
     *      the name to change the attributes name to
     */
    public void setName(String name) {
        this.name.setText(name);
    }
    
    /**
     * Setter method for the return type of this attribute
     * @param type 
     *      the return type to change the attributes return type to 
     */
    public void setType(String type) {
        this.type.setText(type);
    }
    
    /**
     * Setter method for the static status of this attribute
     * @param isStatic 
     *      the boolean value to change this attributes static status to
     */
    public void setIsStatic(boolean isStatic) {
        this.isStatic = isStatic;
        staticCheckBox.setSelected(isStatic);
    }
    
    /**
     * Setter method for the accessibility of this attribute
     * @param access 
     *      the String value to change this attributes accessibility to
     */
    public void setAccess(String access) {
        this.access.setValue(access);
    }
    
    /**
     * Setter method for the abstract status of this method
     * @param isAbstract 
     *      the boolean value to change this methods abstract status to 
     */
    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
        abstractCheckBox.setSelected(isAbstract);
    }
    
    /**
     * Method for adding a parameter to the parameter ArrayList of TextField
     * @param parameter 
     *      the parameter TextField to add to the ArrayList
     */
    public void addParameter(TextField parameter) {
        parameters.add(parameter);
        removeParamButton.setDisable(false);
    }
    
    public String convertToString() {
        String s = "";
        if (access.getValue().equals("public")) {
            s += "+";
        } else if (access.getValue().equals("private")) {
            s += "-";
        } else if (access.getValue().equals("protected")) {
            s += "#";
        }
        if (isStatic) {
            s += "$";
        }
        s += name.getText();
        if (isMethod) {
            s += "(";
            for (int i = 0; i < parameters.size(); i ++) {
                s += ("arg" + (i + 1) + " : " + parameters.get(i).getText());
                if (i != parameters.size() - 1) {
                    s += ", ";
                }
            }
            s += ")";
        }
        s += (" : " + type.getText());
        return s;
    }
    
    /**
     * Getter method for the TextField representing the name of this attribute
     * @return 
     *      the name as a TextField
     */
    public TextField getNameTextField() {
        return name;
    }
    
    /**
     * Getter method for the TextField representing the return type of this attribute
     * @return 
     *      the type as a TextField
     */
    public TextField getTypeTextField() {
        return type;
    }
    
    /**
     * Getter method for the CheckBox representing the static status of this attribute
     * @return 
     *      the static status as a CheckBox
     */
    public CheckBox getStaticCheckBox() {
        return staticCheckBox;
    }
    
    /**
     * Getter method for the ChoiceBox representing the access type of this attribute
     * @return 
     *      the access type as a ChoiceBox
     */
    public ChoiceBox getAccessChoiceBox() {
        return access;
    }
    
    /**
     * Getter method for the CheckBox representing the abstractness of this attribute
     * @return 
     *      the abstractness as a CheckBox
     */
    public CheckBox getAbstractCheckBox() {
        return abstractCheckBox;
    }
    
    /**
     * Helper method for setting the style of non-selected attributes
     */
    public void disableAll() {
        name.getStyleClass().remove("selected_attribute"); name.getStyleClass().add("attribute");
        type.getStyleClass().remove("selected_attribute"); type.getStyleClass().add("attribute");
        staticCheckBox.getStyleClass().remove("selected_attribute"); staticCheckBox.getStyleClass().add("attribute");
        abstractCheckBox.getStyleClass().remove("selected_attribute"); abstractCheckBox.getStyleClass().add("attribute");
        access.getStyleClass().remove("selected_attribute"); access.getStyleClass().add("attribute");
        for (TextField param : parameters) {
            param.getStyleClass().remove("selected_attribute"); param.getStyleClass().add("attribute");
        }
    }
    
    /**
     * Helper method for setting the style of selected attributes
     */
    public void enableAll() {
        name.getStyleClass().remove("attribute"); name.getStyleClass().add("selected_attribute");
        type.getStyleClass().remove("attribute"); type.getStyleClass().add("selected_attribute");
        staticCheckBox.getStyleClass().remove("attribute"); staticCheckBox.getStyleClass().add("selected_attribute");
        abstractCheckBox.getStyleClass().remove("attribute"); abstractCheckBox.getStyleClass().add("selected_attribute");
        access.getStyleClass().remove("attribute"); access.getStyleClass().add("selected_attribute");
        for (TextField param : parameters) {
            param.getStyleClass().remove("attribute"); param.getStyleClass().add("selected_attribute");
        }
    }
    
    public Button getAddParamButton() {
        return addParamButton;
    }
    
    public Button getRemoveParamButton() {
        return removeParamButton;
    }
    
    public HBox getButtons() {
        return buttons;
    }
    
    public void removeParameter() {
        if (!parameters.isEmpty()) {
            parameters.remove(parameters.size() - 1);
        }
        if (parameters.isEmpty()) {
            removeParamButton.setDisable(true);
        }
    }
    
    @Override
    public AttributeDesign clone() {
        AttributeDesign clone = new AttributeDesign(isMethod);
        clone.setName(name.getText());
        clone.setType(type.getText());
        clone.setAccess(access.getValue());
        clone.setIsAbstract(isAbstract);
        clone.setIsStatic(isStatic);
        for (TextField param : parameters) {
            clone.addParameter(new TextField(param.getText()));
        }
        return clone;
    }
}
