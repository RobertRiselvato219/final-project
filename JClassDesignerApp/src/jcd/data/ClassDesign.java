package jcd.data;

import javafx.scene.layout.VBox;
import javafx.scene.control.Label;
import javafx.geometry.Pos;
import java.util.ArrayList;
import javafx.scene.control.TextField;

/**
 * @author Robert Riselvato
 */
public class ClassDesign extends VBox {
    
    // DETERMINES IF THIS DESIGN IS AN INTERFACE
    private boolean isInterface;
    
    // DETERMINES IF THIS DESIGN IS ABSTRACT
    private boolean isAbstract;
    private Label abstractLabel;
    
    // THE NAME OF THIS DESIGN
    private VBox nameVBox;
    private Label name;
    private Label lines1;
    private Label lines2;
    
    // THE NAME OF THE PACKAGE WHERE THIS DESIGN IS LOCATED
    private String packageName;
    private Label packageLabel;
    
    // THE POSITION OF THE DESIGN
    private double x;
    private double y;
    
    // THE ATTRIBUTES OF THIS DESIGN
    private ArrayList<AttributeDesign> variables;
    private ArrayList<AttributeDesign> methods;
    private VBox variablesVBox;
    private VBox methodsVBox;
    
    // THE PARENTS AND INTERFACES OF THIS DESIGN
    private ArrayList<ClassDesign> parents;
    private ArrayList<LineDesign> aggregates;
    private ArrayList<LineDesign> associates;
    
    public ClassDesign() {
        super();
        nameVBox = new VBox();
        name = new Label("");
        packageName = "";
        packageLabel = new Label("");
        variables = new ArrayList<AttributeDesign>();
        methods = new ArrayList<AttributeDesign>();
        parents = new ArrayList<ClassDesign>();
        aggregates = new ArrayList<LineDesign>();
        associates = new ArrayList<LineDesign>();
        abstractLabel = new Label("(abstract)");
        variablesVBox = new VBox();
        methodsVBox = new VBox();
        lines1 = new Label("--------------------------------");
        lines2 = new Label("--------------------------------");
    }
    
    /**
     * Creates a new Design with the position and the boolean value
     * Overloaded constructor
     * @param x
     *      the x position of the rectangle
     * @param y
     *      the y position of the rectangle
     * @param isInterface 
     *      true if the design is an Interface, false if it's a Class
     */
    public ClassDesign(double x, double y, boolean isInterface) {
        this(); this.x = x; this.y = y;
        this.setMinWidth(202); this.setMinHeight(102);
        this.isInterface = isInterface;
        nameVBox.getChildren().add(abstractLabel);
        if (isInterface) {
            nameVBox.getChildren().add(new Label("<<interface>>"));
        }
        VBox lines = new VBox(lines1);
        lines.setAlignment(Pos.CENTER);
        nameVBox.setAlignment(Pos.CENTER);
        nameVBox.getChildren().addAll(name, lines2);
        this.getChildren().add(nameVBox);
        this.getChildren().add(variablesVBox);
        this.getChildren().add(lines);
        this.getChildren().add(methodsVBox);
        this.getStyleClass().add("design_pane");
        isAbstract = false;
        abstractLabel.setVisible(false);
    }
    
    /**
     * Setter method for the name of this design
     * @param n 
     *      the new name of this name
     */
    public void setName(String n) {
        name.setText(n);
    }
    
    /**
     * Getter method for the name of this design
     * @return 
     *      the name of this design as a String
     */
    public String getName() {
        return name.getText();
    }
    
    /**
     * Getter method for the X position of this design
     * @return 
     *      the X position as a Double
     */
    public double getXPosition() {
        return x;
    }
    
    /**
     * Getter method for the Y position of this design
     * @return 
     *      the Y position as a Double
     */
    public double getYPosition() {
        return y;
    }
    
    /**
     * Setter method for the X position of this design
     * @param x 
     *      the new X position of this design
     */
    public void setXPosition(double x) {
        this.x = x;
    }
    
    /**
     * Setter method for the Y position of this design
     * @param y 
     *      the new Y position of this design
     */
    public void setYPosition(double y) {
        this.y = y;
    }
    
    /**
     * Getter method for the package of this design
     * @return 
     *      the package as a String
     */
    public String getPackage() {
        return packageName;
    }
    
    /**
     * Setter method for the package of this design
     * @param s 
     *      the new package of this design
     */
    public void setPackage(String s) {
        packageName = s;
        packageLabel.setText(s);
    }
    
    /**
     * Getter method for the status of this design
     * @return 
     *      true if the design is an interface, false otherwise
     */
    public boolean isInterface() {
        return isInterface;
    }
    
    /**
     * Getter method for the variables of this design
     * @return 
     *      the variables of thus design as an ArrayList of AttributeDesign
     */
    public ArrayList<AttributeDesign> getVariables() {
        return variables;
    }
    
    /**
     * Getter methods for the methods of this design
     * @return 
     *      the methods of this design as an ArrayList of AttributeDesign
     */
    public ArrayList<AttributeDesign> getMethods() {
        return methods;
    }
    
    /**
     * Method for adding a variable to the ArrayList of AttributeDesign
     * @param variable 
     *      the variable to add to the ArrayList
     */
    public void addVariable(AttributeDesign variable) {
        variables.add(variable);
        variablesVBox.getChildren().add(new Label(variable.convertToString()));
        this.setMinSize(this.prefWidth(-1), this.prefHeight(-1));
    }
    
    /**
     * Method for adding a method to the ArrayList of AttributeDesign
     * @param method 
     *      the method to add to the ArrayList
     */
    public void addMethod(AttributeDesign method) {
        methods.add(method);
        methodsVBox.getChildren().add(new Label(method.convertToString()));
        this.setMinSize(this.prefWidth(-1), this.prefHeight(-1));
    }
    
    /**
     * Getter method for the ArrayList of this designs parents
     * @return 
     *      the parents of this design as an ArrayList of ClassDesign
     */
    public ArrayList<ClassDesign> getParents() {
        return parents;
    }
    
    /**
     * Method for adding a parent to this design
     * @param design 
     *      the ClassDesign parent to be added to this designs ArrayList of parents
     */
    public void addParent(ClassDesign design) {
        parents.add(design);
    }
    
    /**
     * Getter method for the abstractness of this design
     * @return 
     *      true if this design is abstract, false otherwise
     */
    public boolean isAbstract() {
        return isAbstract;
    }
    
    /**
     * Setter method for the abstractness of this design
     * @param isAbstract 
     *      the abstractness to change the boolean value to 
     */
    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
        abstractLabel.setVisible(isAbstract);
    }
    
    /**
     * Calculates the coordinates of the center of this design
     * @return 
     *      the coordinates within a primitive array of Doubles
     */
    public double[] getCenter() {
        double width = (x + (Math.max(getMinWidth(), prefWidth(-1)) / 2));
        double height = (y + (Math.max(getMinHeight(), prefHeight(-1)) / 2));
        double[] center = {width, height};
        return center;
    }
    
    public void reloadVariablesVBox() {
        variablesVBox.getChildren().clear();
        for (AttributeDesign v : variables) {
            variablesVBox.getChildren().add(new Label(v.convertToString()));
        }
        setMinSize(Math.max(prefWidth(-1), getWidth()), Math.max(prefHeight(-1), getHeight()));
    }
    
    public void reloadMethodsVBox() {
        methodsVBox.getChildren().clear();
        for (AttributeDesign m : methods) {
            methodsVBox.getChildren().add(new Label(m.convertToString()));
        }
        setMinSize(Math.max(prefWidth(-1), getWidth()), Math.max(prefHeight(-1), getHeight()));
    }
    
    public void updateIsAbstract() {
        boolean a = false;
        for (AttributeDesign m : methods) {
            if (m.isAbstract()) {
                a = true;
            }
        }
        setIsAbstract(a);
    }
    
    @Override
    public ClassDesign clone() {
        ClassDesign clone = new ClassDesign(x, y, isInterface);
        clone.setName(name.getText());
        clone.setPackage(packageName);
        clone.setIsAbstract(isAbstract);
        double width = this.getMinWidth();
        double height = this.getMinHeight();
        for (AttributeDesign variable : variables) {
            clone.addVariable(variable.clone());
        }
        for (AttributeDesign method : methods) {
            clone.addMethod(method.clone());
        }
        clone.setMinSize(width, height);
        return clone;
    }
    
    public Label getNameLabel() {
        return name;
    }
    
    public Label getPackageLabel() {
        return packageLabel;
    }
    
    public ArrayList<LineDesign> getLines() {
        ArrayList<LineDesign> lines = new ArrayList<LineDesign>();
        lines.addAll(aggregates);
        lines.addAll(associates);
        return lines;
    }
    
    public ArrayList<LineDesign> removeAggregates() {
        ArrayList<LineDesign> remove = new ArrayList<LineDesign>();
        for (LineDesign line : aggregates) {
            boolean exists = false;
            for (AttributeDesign v : variables) {
                if (line.getStartClass().getName().equals(v.getType())) {
                    exists = true;
                }
            }
            for (ClassDesign c : parents) {
                if (line.getStartClass().getName().equals(c.getName())) {
                    exists = false;
                }
            } 
            if (!exists) {
                remove.add(line);
            }
        }
        aggregates.removeAll(remove);
        return remove;
    }
    
    public ArrayList<LineDesign> removeAssociates() {
        ArrayList<LineDesign> remove = new ArrayList<LineDesign>();
        for (LineDesign line : associates) {
            boolean exists = false;
            for (AttributeDesign m : methods) {
                if (line.getEndClass().getName().equals(m.getType())) {
                    exists = true;
                }
                for (TextField p : m.getParemeters()) {
                    if (line.getEndClass().getName().equals(p.getText())) {
                        exists = true;
                    }
                }
            }
            for (ClassDesign c : parents) {
                if (line.getEndClass().getName().equals(c.getName())) {
                    exists = false;
                }
            }
            if (!exists) {
                remove.add(line);
            }
        }
        associates.removeAll(remove);
        return remove;
    }
    
    public ArrayList<LineDesign> addAggregates(ArrayList<ClassDesign> designs) {
        ArrayList<LineDesign> add = new ArrayList<LineDesign>();
        for (AttributeDesign v : variables) {
            boolean exists = false;
            ClassDesign aggregate = new ClassDesign();
            ArrayList<LineDesign> remove = new ArrayList<LineDesign>();
            boolean create = true;
            for (ClassDesign design : designs) {
                if (design.getName().equals(v.getType()) && !parents.contains(design)) {
                    exists = true;
                    aggregate = design;
                }
            }
            if (exists) {
                for (LineDesign line : aggregates) {
                    if (line.getStartClass().getName().equals(v.getType())) {
                        create = false;
                    }
                }
                for (LineDesign line : associates) {
                    if (line.getEndClass().getName().equals(v.getType())) {
                        create = false;
                        line.resetLine(line.getEndClass(), line.getStartClass(), "aggregate");
                        aggregates.add(line);
                        remove.add(line);
                    }
                }
                associates.removeAll(remove);
            }
            if (exists && create) {
                add.add(new LineDesign(aggregate, this, "aggregate"));
            }
        }
        aggregates.addAll(add);
        return add;
    }
    
    public ArrayList<LineDesign> addAssociates(ArrayList<ClassDesign> designs) {
        ArrayList<LineDesign> add = new ArrayList<LineDesign>();
        for (AttributeDesign m : methods) {
            boolean exists = false;
            ArrayList<ClassDesign> associate = new ArrayList<ClassDesign>();
            for (ClassDesign design : designs) {
                if (design.getName().equals(m.getType()) && !parents.contains(design)) {
                    exists = true;
                    associate.add(design);
                }
                for (TextField p : m.getParemeters()) {
                    if (design.getName().equals(p.getText()) && !parents.contains(design)) {
                        exists = true;
                        associate.add(design);
                    }
                }
            }
            ArrayList<ClassDesign> alreadyExists = new ArrayList<ClassDesign>();
            if (exists) {
                for (ClassDesign c : associate) {
                    for (LineDesign line : aggregates) {
                        if (line.getStartClass().equals(c)) {
                            alreadyExists.add(c);
                        }
                    }
                    for (LineDesign line : associates) {
                        if (line.getEndClass().equals(c)) {
                            alreadyExists.add(c);
                        }
                    }
                }
                associate.removeAll(alreadyExists);
                for (ClassDesign c : associate) {
                    add.add(new LineDesign(this, c, "associated"));
                }
            }
        }
        associates.addAll(add);
        return add;
    }
}
