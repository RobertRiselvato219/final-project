package jcd.data;

import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Rotate;

/**
 * @author Rob
 */
public class LineDesign {
    
    // THE LINE OF THIS DESIGN
    private Polyline line;
    
    // THE POINTS MAKING UP THE LINE
    private double[] points;
    
    // THE CLASSES WHERE THE LINE BEGINS AND ENDS
    private ClassDesign startClass;
    private ClassDesign endClass;
    
    // THE ARROW OF THIS LINE
    private Shape arrow;
    
    // RELATIONSHIP 
    String relationship;
    
    /**
     * Constructs a new line with the given classes
     * Overloaded constructor
     * @param c1
     *      the class from which the line begins
     * @param c2 
     *      the class at which the line ends
     */
    public LineDesign(ClassDesign c1, ClassDesign c2, String relation) {
        
        double segmentsX = (c2.getXPosition() + c2.prefWidth(-1) - c1.getCenter()[0]) / 4;
        double segmentsY = (c2.getCenter()[1] - c1.getCenter()[1]) / 4;
        
        points = new double[10];
        
        points[0] = c1.getCenter()[0];
        points[1] = c1.getCenter()[1];
        
        points[2] = (c1.getCenter()[0] + segmentsX);
        points[3] = (c1.getCenter()[1] + segmentsY);
        
        points[4] = (c1.getCenter()[0] + (segmentsX * 2));
        points[5] = (c1.getCenter()[1] + (segmentsY * 2));
        
        points[6] = (c1.getCenter()[0] + (segmentsX * 3));
        points[7] = (c1.getCenter()[1] + (segmentsY * 3));
        
        points[8] = c2.getXPosition() + c2.prefWidth(-1);
        points[9] = c2.getCenter()[1];
        
        startClass = c1;
        endClass = c2;
        
        line = new Polyline(points);
        line.setStrokeWidth(5);
        
        setArrowRelation(relation);
    }
    
    public LineDesign(ClassDesign c1, ClassDesign c2, double[] p, String relation) {
        startClass = c1;
        endClass = c2;
        points = new double[10];
        for (int i = 0; i < 10; i++) {
            points[i] = p[i];
        }
        
        
        line = new Polyline(points);
        line.setStrokeWidth(5);
        
        relationship = relation;
        
        if (relation.equals("extends")) {
            arrow = new Polygon(new double[]{
                0.0, 0.0,
                21.0, 0.0,
                0.0, 21.0
            });
        } else if (relation.equals("aggregate")) {
            arrow = new Rectangle(14, 14);
        } else if (relation.equals("associated")) {
            arrow = new Polygon(new double[]{
                0.0, 0.0,
                28.0, 0.0,
                3.0, 3.0,
                0.0, 28.0
            });
        }
        arrow.relocate(points[8], points[9]);
        placeArrow(points[8], points[9]);
    }
    
    /**
     * Helper method for finding the closest point on the line
     * @param x
     *      the X value of the MouseEvent
     * @param y
     *      the Y value of the MouseEvent
     * @return 
     *      the indices of the coordinates of the closest point as a primitive
     *      array of Integers
     */
    public int[] getClosestPoint(double x, double y) {
        int[] min = {2, 3};
        double minDistance = getDistance(x, points[2], y, points[3]);
        for (int i = 4; i < 10; i = i + 2) {
            if (minDistance > getDistance(x, points[i], y, points[i + 1])) {
                minDistance = getDistance(x, points[i], y, points[i + 1]);
                min[0] = i;
                min[1] = (i + 1);
            }
        }
        return min;
    }
    
    /**
     * Helper method for calculating the distance between two coordinates
     * @param x1
     *      the first X coordinate
     * @param x2
     *      the second X coordinate
     * @param y1
     *      the first Y coordinate
     * @param y2
     *      the second Y coordinate
     * @return 
     *      the distance as a Double
     */
    private double getDistance(double x1, double x2, double y1, double y2) {
        return Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
    }
    
    /**
     * Creates a new line with the edited point
     * @param point
     *      the indices of the point to be edited
     * @param value 
     *      the new coordinates of the point
     */
    public void setPoint(int[] point, double[] value) {
        if (point[0] == 8 && point[1] == 9) {
            placeArrow(value[0], value[1]);
        } else {
            points[point[0]] = value[0];
            points[point[1]] = value[1]; 
        }
        line = new Polyline(points);
        line.setStrokeWidth(5);
    }
    
    /**
     * Getter method for the line itself
     * @return 
     *      the line as a Polyline
     */
    public Polyline getLine() {
        return line;
    }
    
    /**
     * Getter method for the starting class of this line
     * @return 
     *      the starting class as a ClassDesign
     */
    public ClassDesign getStartClass() {
        return startClass;
    }
    
    /**
     * Getter method for the ending class of this line
     * @return 
     *      the ending class as a ClassDesign
     */
    public ClassDesign getEndClass() {
        return endClass;
    }
    
    /**
     * Getter method for the arrow of this line
     * @return 
     *      the arrow as a Shape
     */
    public Shape getArrow(){
        return arrow;
    }
    
    /**
     * Helper method for determining the orientation of the arrow if it's a triangle
     * @param x
     *      the X coordinate of the arrow
     * @param y 
     *      the Y coordinate of the arrow
     */
    private void setArrowOrientation(double x, double y) {
        arrow.getTransforms().clear();
        if (x == endClass.getXPosition()) {
            arrow.getTransforms().add(new Rotate(135, 0, 0));
        } else if (x == endClass.getXPosition() + Math.max(endClass.getMinWidth(), endClass.prefWidth(-1))) {
            arrow.getTransforms().add(new Rotate(-45, 0, 0));
        } else if (y == endClass.getYPosition() + Math.max(endClass.getMinHeight(), endClass.prefHeight(-1))) {
            arrow.getTransforms().add(new Rotate(45, 0, 0));
        } else if (y == endClass.getYPosition()) {
            arrow.getTransforms().add(new Rotate(-135, 0, 0));
        }
    }
    
    /**
     * Getter method for the points of the line
     * @return 
     *      the points as a primitive array of Doubles
     */
    public double[] getPoints() {
        return points;
    }
    
    /**
     * Places the last point and the correlated arrow on the boundary of the ending class
     * @param x
     *      the X coordinate of the MouseEvent
     * @param y 
     *      the Y coordinate of the MouseEvent
     */
    public void placeArrow(double x, double y) {
        // EAST & WEST
        double yEW;
        if (y < endClass.getYPosition()) {
            yEW = endClass.getYPosition();
        } else if (y > endClass.getYPosition() + Math.max(endClass.getMinHeight(), endClass.prefHeight(-1))) {
            yEW = endClass.getYPosition() + Math.max(endClass.getMinHeight(), endClass.prefHeight(-1));
        } else {
            yEW = y;
        }
        double[] west = {endClass.getXPosition(), yEW};
        double[] east = {endClass.getXPosition() + Math.max(endClass.getMinWidth(), endClass.prefWidth(-1)), yEW};
        
        // NORTH & SOUTH
        double xNS;
        if (x < endClass.getXPosition()) {
            xNS = endClass.getXPosition();
        } else if (x > endClass.getXPosition() + Math.max(endClass.getMinWidth(), endClass.prefWidth(-1))) {
            xNS = endClass.getXPosition() + Math.max(endClass.getMinWidth(), endClass.prefWidth(-1));
        } else {
            xNS = x;
        }
        double[] north = {xNS, endClass.getYPosition()};
        double[] south = {xNS, endClass.getYPosition() + Math.max(endClass.getMinHeight(), endClass.prefHeight(-1))};
        
        double minDis = getDistance(x, west[0], y, west[1]);
        double[] min = west;
        double lagX = -14;
        double lagY = 0;
        if (minDis > getDistance(x, east[0], y, east[1])) {
            minDis = getDistance(x, east[0], y, east[1]);
            min = east;
            lagX = 14;
            lagY = 0;
        }
        if (minDis > getDistance(x, north[0], y, north[1])) {
            minDis = getDistance(x, north[0], y, north[1]);
            min = north;
            lagX = 0;
            lagY = -14;
        }
        if (minDis > getDistance(x, south[0], y, south[1])) {
            minDis = getDistance(x, south[0], y, south[1]);
            min = south;
            lagX = 0;
            lagY = 14;
        }
        
        points[8] = min[0] + lagX;
        points[9] = min[1] + lagY;
        arrow.relocate(min[0], min[1]);
        setArrowOrientation(min[0], min[1]);
    }
    
    public LineDesign clone(ClassDesign c1, ClassDesign c2) {
        return new LineDesign(c1, c2, points, relationship);
    }
    
    public void setArrowRelation(String s) {
        relationship = s;
        if (s.equals("extends")) {
            arrow = new Polygon(new double[]{
                0.0, 0.0,
                21.0, 0.0,
                0.0, 21.0
            });
        } else if (s.equals("aggregate")) {
            arrow = new Rectangle(14, 14);
        } else if (s.equals("associated")) {
            arrow = new Polygon(new double[]{
                0.0, 0.0,
                28.0, 0.0,
                3.0, 3.0,
                0.0, 28.0
            });
        }
        arrow.relocate(points[8], points[9]);
        placeArrow(points[8], points[9]);
    }
    
    public void resetLine(ClassDesign c1, ClassDesign c2, String relation) {
        double segmentsX = (c2.getXPosition() + c2.prefWidth(-1) - c1.getCenter()[0]) / 4;
        double segmentsY = (c2.getCenter()[1] - c1.getCenter()[1]) / 4;
        
        points = new double[10];
        
        points[0] = c1.getCenter()[0];
        points[1] = c1.getCenter()[1];
        
        points[2] = (c1.getCenter()[0] + segmentsX);
        points[3] = (c1.getCenter()[1] + segmentsY);
        
        points[4] = (c1.getCenter()[0] + (segmentsX * 2));
        points[5] = (c1.getCenter()[1] + (segmentsY * 2));
        
        points[6] = (c1.getCenter()[0] + (segmentsX * 3));
        points[7] = (c1.getCenter()[1] + (segmentsY * 3));
        
        points[8] = c2.getXPosition() + c2.prefWidth(-1);
        points[9] = c2.getCenter()[1];
        
        startClass = c1;
        endClass = c2;
        
        line = new Polyline(points);
        line.setStrokeWidth(5);
        
        setArrowRelation(relation);
    }
    
    public String getRelation() {
        return relationship;
    }
}
