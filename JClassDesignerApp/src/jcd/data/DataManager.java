package jcd.data;

import saf.components.AppDataComponent;
import saf.AppTemplate;
import java.util.ArrayList;
import jcd.gui.Workspace;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author Robert Riselvato
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    private ArrayList<ClassDesign> designs;
    private ArrayList<LineDesign> lines;

    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        
        designs = new ArrayList<ClassDesign>();
        lines = new ArrayList<LineDesign>();
    }
    
    public DataManager() { designs = new ArrayList<ClassDesign>(); lines = new ArrayList<LineDesign>(); }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        designs.clear();
        lines.clear();
    }
    
    /**
     * Adds a design to the array list
     * @param design 
     *      the design to add
     */
    public void addDesign(ClassDesign design) {
        designs.add(design);
    }
    
    /**
     * Provides the array list of designs
     * @return 
     *      the designs as an ArrayList<ClassDesign> 
     */
    public ArrayList<ClassDesign> getDesigns() {
        return designs;
    }
    
    /**
     * Helper method for reloading the workspace
     */
    public void reloadWorkspace() {
        if (app != null) {
            ((Workspace) app.getWorkspaceComponent()).reloadWorkspace();
        }
    }
    
    public ArrayList<LineDesign> getLines() {
        return lines;
    }
    
    public void addLine(LineDesign line) {
        lines.add(line);
    }
    
    @Override
    public DataManager clone() {
        DataManager clone;
        try {
            clone = new DataManager(app);
            for (ClassDesign design : designs) {
                clone.addDesign(design.clone());
            }
            for (int i = 0; i < designs.size(); i++) {
                for (ClassDesign parent : designs.get(i).getParents()) {
                    clone.getDesigns().get(i).addParent(findClone(parent, clone.getDesigns()));
                }
            }
            for (LineDesign line : lines) {
                ClassDesign startClass = findClone(line.getStartClass(), clone.getDesigns());
                ClassDesign endClass = findClone(line.getEndClass(), clone.getDesigns());
                LineDesign lineClone = line.clone(startClass, endClass);
                clone.addLine(line.clone(findClone(line.getStartClass(), clone.getDesigns()), 
                        findClone(line.getEndClass(), clone.getDesigns())));
            }
            return clone;
        } catch (Exception e) {
            return null;
        }
    }
    
    public ClassDesign findClone(ClassDesign design, ArrayList<ClassDesign> clones) {
        if (!clones.isEmpty()) {
            int i = 0;
            while (!(design.getName().equals(clones.get(i).getName()) 
                    && design.getPackage().equals(clones.get(i).getPackage()))) {
                i++;
            }
            return clones.get(i);
        } else {
            return null;
        }
    }
    
    public void updateData(DataManager newData) {
        designs = newData.getDesigns();
        lines = newData.getLines();
    }
    
    public ClassDesign getDesign(String name) {
        for (ClassDesign design : designs) {
            if (design.getName().equals(name) && design.getPackage().equals("")) {
                return design;
            }
        }
        return null;
    }
    
    public ClassDesign getDesignSubstring(String substring) {
        for (ClassDesign design : designs) {
            if (design.getName().length() > 1 && design.getPackage().equals("")) {
                if (design.getName().substring(0, design.getName().length() - 1).equals(substring)) {
                    return design;
                }
            }
        }
        return null;
    }
}
