package jcd.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import jcd.data.DataManager;
import jcd.data.ClassDesign;
import javax.json.JsonArrayBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jcd.data.AttributeDesign;
import javax.json.JsonObjectBuilder;
import javafx.scene.control.TextField;
import java.util.HashSet;
import java.lang.Package;
import java.lang.Class;
import java.util.Scanner;
import saf.ui.AppYesNoCancelDialogSingleton;
import jcd.data.LineDesign;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author Robert Riselvato
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_WIDTH = "width";
    static final String JSON_HEIGHT = "height";
    static final String JSON_IS_INTERFACE = "is_interface";
    static final String JSON_NAME = "name";
    static final String JSON_PACKAGE_NAME = "package_name";
    static final String JSON_DESIGNS = "designs";
    static final String JSON_VARIABLES = "variables";
    static final String JSON_METHODS = "methods";
    static final String JSON_PARENTS = "parents";
    static final String JSON_IS_ABSTRACT = "is_abstract";
    
    static final String JSON_IS_METHOD = "is_method";
    static final String JSON_ATTRIBUTE_NAME = "attribute_name";
    static final String JSON_TYPE = "type";
    static final String JSON_IS_STATIC = "is_static";
    static final String JSON_ACCESS = "access";
    static final String JSON_ATTRIBUTE_IS_ABSTRACT = "attribute_is_abstract";
    static final String JSON_PARAMETERS = "parameters";
    
    static final String JSON_PARAMETER_TYPE = "parameter_type";
    
    static final String JSON_PARENT_NAME = "parent_name";
    static final String JSON_PARENT_PACKAGE = "parent_package";

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        
        DataManager dataManager = (DataManager) data;
        
        JsonArrayBuilder arraybuilder = Json.createArrayBuilder();
        ArrayList<ClassDesign> designs = dataManager.getDesigns();
        if (!designs.isEmpty()) {
            for (ClassDesign design : designs) {
                String name = design.getName();
                String packageName = design.getPackage();
                double x = design.getXPosition();
                double y = design.getYPosition();
                boolean isInterface = design.isInterface();
                JsonArray variables = attributeJsonBuilder(design.getVariables());
                JsonArray methods = attributeJsonBuilder(design.getMethods());
                JsonArray parents = parentJsonBuilder(design.getParents());
                boolean isAbstract = design.isAbstract();
                double height = design.getMinHeight();
                double width = design.getMinWidth();
            
                JsonObject designJson = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_PACKAGE_NAME, packageName)
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_IS_INTERFACE, isInterface)
                        .add(JSON_VARIABLES, variables)
                        .add(JSON_METHODS, methods)
                        .add(JSON_PARENTS, parents)
                        .add(JSON_IS_ABSTRACT, isAbstract)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_WIDTH, width)
                        .build();
                arraybuilder.add(designJson);
            }
        }
        JsonArray designArray = arraybuilder.build();
        
        JsonObject dataJson = Json.createObjectBuilder()
                .add(JSON_DESIGNS, designArray)
                .build();
        
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataJson);
	jsonWriter.close();
        
        OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataJson);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
      
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager) data;
	dataManager.reset();
        
        JsonObject json = loadJSONFile(filePath);
        
        JsonArray jsonDesigns = json.getJsonArray(JSON_DESIGNS);
        for (int i = 0; i < jsonDesigns.size(); i++) {
            JsonObject jsonDesign = jsonDesigns.getJsonObject(i);
            ClassDesign design = loadDesign(jsonDesign);
            dataManager.addDesign(design);
        }
        //dataManager.reloadWorkspace();
        for (int i = 0; i < jsonDesigns.size(); i++) {
            JsonObject jsonDesign = jsonDesigns.getJsonObject(i);
            ClassDesign design = dataManager.getDesigns().get(i);
            JsonArray parents = jsonDesign.getJsonArray(JSON_PARENTS);
            if (!parents.isEmpty()) {
                for (int j = 0; j < parents.size(); j++) {
                    JsonObject parentJson = parents.getJsonObject(j);
                    int d = 0;
                    while (!parentJson.getString(JSON_PARENT_NAME).equals(dataManager.getDesigns().get(d).getName()) || 
                            !parentJson.getString(JSON_PARENT_PACKAGE).equals(dataManager.getDesigns().get(d).getPackage())) {
                        d++;
                    }
                    design.addParent(dataManager.getDesigns().get(d));
                    LineDesign line = new LineDesign(design, dataManager.getDesigns().get(d), "extends");
                    dataManager.addLine(line);
                }
            }
            for (LineDesign line : design.addAggregates(dataManager.getDesigns())) {
                dataManager.addLine(line);
            }
            for (LineDesign line : design.addAssociates(dataManager.getDesigns())) {
                dataManager.addLine(line);
            }
        }
        dataManager.reloadWorkspace();
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        HashSet<String> packages = new HashSet<String>();
        File file = new File(filePath);
        file.mkdir();
        File source = new File(filePath + "/src/");
        (new File(filePath + "/nbproject/")).mkdirs();
        source.mkdir();
        for (ClassDesign design : ((DataManager) data).getDesigns()) {
            String packageString = design.getPackage();
            try {
                Class.forName(packageString + "." + design.getName());
            } catch (ClassNotFoundException cnfe) {
                String p = packageString.replaceAll("\\.", "/");
                packages.add(p);
            }  
        } 
        String defaultPackage = filePath.substring(filePath.lastIndexOf("\\") + 1, filePath.length());
        buildNetBeansProject(defaultPackage, filePath);
        for (String packageFileString : packages) {
            (new File(filePath + "/src/" + packageFileString.replaceAll("\\.", "/"))).mkdirs();
            if (packageFileString.isEmpty()) {
                //(new File(filePath + "/src/" + defaultPackage.toLowerCase())).mkdirs();
            }
        }
        for (ClassDesign design : ((DataManager) data).getDesigns()) {
            if (design.getPackage().isEmpty()) {
                //design.setPackage(defaultPackage.toLowerCase());
            }
            if (Package.getPackage(design.getPackage()) == null) {
                File designFile = new File(filePath + "/src/" + design.getPackage().replaceAll("\\.", "/") + "/" + design.getName() + ".java");
                PrintWriter pw = new PrintWriter(designFile.getPath());
                pw.write(sourceCodeString(design, (DataManager) data, defaultPackage.toLowerCase()));
                pw.close();
            }
        }
    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
    
    /**
     * Helper method for creating a double value from a component of a JsonObject
     * @param json
     *      the JsonObject from which the component is from
     * @param dataName
     *      the name of the component to be converted into a double
     * @return 
     *      the component of the JsonObject as a double
     */
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    /**
     * Helper method for creating a ClassDesign from a JsonObject
     * @param jsonDesign
     *      the JsonObject to be converted into a ClassDesign
     * @return 
     *      the JsonObject as a ClassDesign
     */
    public ClassDesign loadDesign(JsonObject jsonDesign) {
        double x = getDataAsDouble(jsonDesign, JSON_X);
        double y = getDataAsDouble(jsonDesign, JSON_Y);
        boolean isInterface = jsonDesign.getBoolean(JSON_IS_INTERFACE);
        ClassDesign design = new ClassDesign(x, y, isInterface);
        design.setName(jsonDesign.getString(JSON_NAME));
        design.setPackage(jsonDesign.getString(JSON_PACKAGE_NAME));
        JsonArray variables = jsonDesign.getJsonArray(JSON_VARIABLES);
        JsonArray methods = jsonDesign.getJsonArray(JSON_METHODS);
        if (!variables.isEmpty()) {
            for (int i = 0; i < variables.size(); i++) {
                design.addVariable(loadAttribute(variables.getJsonObject(i)));
            }
        }
        if (!methods.isEmpty()) {
            for (int i = 0; i < methods.size(); i++) {
                design.addMethod(loadAttribute(methods.getJsonObject(i)));
            }
        }
        design.setIsAbstract(jsonDesign.getBoolean(JSON_IS_ABSTRACT));
        //design.setMinHeight(getDataAsDouble(jsonDesign, JSON_HEIGHT));
        //design.setMinWidth(getDataAsDouble(jsonDesign, JSON_WIDTH));
        
        return design;
    }
    
    /**
     * Creates a JsonArray of the attributes of a design
     * @param attributes
     *      the attributes of a given design
     * @return 
     *      the attributes as a JsonArray
     */
    public JsonArray attributeJsonBuilder(ArrayList<AttributeDesign> attributes) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        if (!attributes.isEmpty()) {
            for (AttributeDesign attribute : attributes) {
                JsonObjectBuilder builder = Json.createObjectBuilder();
                String name = attribute.getName(); builder.add(JSON_ATTRIBUTE_NAME, name);
                String type = attribute.getType(); builder.add(JSON_TYPE, type);
                String access = attribute.getAccess(); builder.add(JSON_ACCESS, access);
                boolean isStatic = attribute.isStatic(); builder.add(JSON_IS_STATIC, isStatic);
                boolean isMethod = attribute.isMethod();
                if (isMethod) {
                    boolean isAbstract = attribute.isAbstract();
                    builder.add(JSON_ATTRIBUTE_IS_ABSTRACT, isAbstract);
                    JsonArray parameters = parameterJsonBuilder(attribute.getParemeters());
                    builder.add(JSON_PARAMETERS, parameters);
                }
                builder.add(JSON_IS_METHOD, isMethod);
                arrayBuilder.add(builder.build());
            }
        }
        return arrayBuilder.build();
    }
    
    /**
     * Helper method for creating a JsonArray with the parameters of a method
     * @param parameters
     *      the parameters to be converted into a JsonArray
     * @return 
     *      the parameters as a JsonArray
     */
    public JsonArray parameterJsonBuilder(ArrayList<TextField> parameters) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        if (!parameters.isEmpty()) {
            for (TextField parameter : parameters) {
                String type = parameter.getText();
                JsonObject paramJson = Json.createObjectBuilder()
                        .add(JSON_PARAMETER_TYPE, type)
                        .build();
                arrayBuilder.add(paramJson);
            }
        }
        return arrayBuilder.build();
    }
    
    /**
     * Helper method for making a TextField parameter from a JsonObject
     * @param paramJson
     *      the JsonObject to convert to a TextField
     * @return 
     *      the JsonObject as a TextField
     */
    public TextField loadParameter(JsonObject paramJson) {
        String type = paramJson.getString(JSON_PARAMETER_TYPE);
        return new TextField(type);
    }
    
    /**
     * Helper method for creating an AttributeDesign from a JsonObject
     * @param attributeJson
     *      the JsonObject to be converted into an AttributeDesign
     * @return 
     *      the JsonObject as an AttributeDesign
     */
    public AttributeDesign loadAttribute(JsonObject attributeJson) {
        boolean isMethod = attributeJson.getBoolean(JSON_IS_METHOD);
        String name = attributeJson.getString(JSON_ATTRIBUTE_NAME);
        String type = attributeJson.getString(JSON_TYPE);
        String access = attributeJson.getString(JSON_ACCESS);
        boolean isStatic = attributeJson.getBoolean(JSON_IS_STATIC);
        AttributeDesign attribute = new AttributeDesign(isMethod);
        attribute.setName(name);
        attribute.setType(type);
        attribute.setAccess(access);
        attribute.setIsStatic(isStatic);
        if (isMethod) {
            boolean isAbstract = attributeJson.getBoolean(JSON_ATTRIBUTE_IS_ABSTRACT);
            JsonArray parameters = attributeJson.getJsonArray(JSON_PARAMETERS);
            attribute.setIsAbstract(isAbstract);
            if (!parameters.isEmpty()) {
                for (int i = 0; i < parameters.size(); i++) {
                    attribute.addParameter(loadParameter(parameters.getJsonObject(i)));
                }
            }
        }
        return attribute;
    }
    
    /**
     * Helper method for creating a JsonArray of the parents of a design
     * @param parents
     *      the ArrayList of parents for to made into a JsonArray
     * @return 
     *      the parents as a JsonArray
     */
    public JsonArray parentJsonBuilder(ArrayList<ClassDesign> parents) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        if (!parents.isEmpty()) {
            for (ClassDesign parent : parents) {
                JsonObject parentJson = Json.createObjectBuilder()
                        .add(JSON_PARENT_NAME, parent.getName())
                        .add(JSON_PARENT_PACKAGE, parent.getPackage())
                        .build();
                arrayBuilder.add(parentJson);
            }
        }
        return arrayBuilder.build();
    }
    
    /**
     * Helper method for creating a string representing the source code of a given design
     * @param design
     *      the design being exported to source code
     * @param defaultPackage
     *      the default String if a design has no package
     * @return 
     *      the source code as a String
     */
    public String sourceCodeString(ClassDesign design, DataManager data, String defaultPackage) {
        String s = "";
        if (!design.getPackage().equals("")) {
            s = "package " + design.getPackage() + ";\n\n";
        } else {
            //s = "package " + defaultPackage + ";\n\n";
        }
        ClassDesign parentClass = null;
        for (ClassDesign parent : design.getParents()) {
            if (!parent.getPackage().equals("") && !parent.getPackage().equals(design.getPackage())) {
                s += ("import " + parent.getPackage() + "." + parent.getName() + ";\n");
            }
            if (!parent.isInterface()) {
                parentClass = parent;
            }
        }
        for (AttributeDesign variable : design.getVariables()) {
            s += searchPackages(variable.getType(), design, data);
        }
        for (AttributeDesign method : design.getMethods()) {
            s += searchPackages(method.getType(), design, data);
            for (TextField param : method.getParemeters()) {
                s += searchPackages(param.getText(), design, data);
            }
        }
        s += ("\npublic ");
        if (design.isAbstract()) {
            s += "abstract ";
        }
        if (design.isInterface()) {
            s += "interface ";
        } else {
            s += "class ";
        }
        s += (design.getName() + " ");
        if (parentClass != null) {
            s += ("extends " + parentClass.getName());
            if (design.getParents().size() > 1) {
                s += ", implements ";
                for (int i = 0; i < design.getParents().size(); i++) {
                    ClassDesign parent = design.getParents().get(i);
                    if (!parentClass.equals(parent)) {
                        s += parent.getName();
                        if (i != design.getParents().size() - 1) {
                            s += ",";
                        }
                        s += " ";
                    }
                }
            }
        } else {
            if (!design.getParents().isEmpty()) {
                s += "implements ";
                for (int i = 0; i < design.getParents().size(); i++) {
                    ClassDesign parent = design.getParents().get(i);
                    s += parent.getName();
                    if (i != design.getParents().size() - 1) {
                        s += ",";
                    } 
                    s += " ";
                }
            }
        }
        s += "{\n";
        for (AttributeDesign v : design.getVariables()) {
            s += AttributeToString(v);
        }
        for (AttributeDesign m : design.getMethods()) {
            s += AttributeToString(m);
        }
        s += "\n}";
        return s; 
    }
    
    /**
     * Helper method for searching for the correct package to import for code compiling
     * @param type
     *      the return type of the attribute
     * @param design
     *      the design who needs imports
     * @param data
     *      the data of all the designs
     * @return 
     *      the String representing the correct package
     */
    public String searchPackages(String type, ClassDesign design, DataManager data) {
        boolean found = false;
        String packageString = "";
        if (!type.isEmpty()) {
            if (Character.isUpperCase(type.charAt(0)) && !type.equals("String") && !type.toLowerCase().equals("void")) {
                for (ClassDesign d : data.getDesigns()) {
                    if (type.equals(d.getName()) && !d.getPackage().equals(design.getPackage())) {
                        packageString += ("import " + d.getPackage() + "." + d.getName() + ";\n");
                        found = true;
                    }
                }
                if (!found) {
                    int numFound = 0;
                    ArrayList<String> packagesFound = new ArrayList<String>();
                    for (Package p : Package.getPackages()) {
                        try {
                            Class.forName(p.getName() + "." + type);
                            numFound++;
                            packagesFound.add(p.getName() + "." + type);
                        } catch (ClassNotFoundException cnfe) {}
                    }
                    while (numFound > 1) {
                        AppYesNoCancelDialogSingleton.getSingleton().show("Multiple Imports Available", 
                                "Would you like to import " + packagesFound.get(0) + " to " + design.getName() + ".java?");
                        if (AppYesNoCancelDialogSingleton.getSingleton().getSelection().equals("Yes")) {
                            numFound = 1; 
                        } else {
                            numFound--;
                            packagesFound.remove(0);
                        }
                    }
                    if (numFound == 1) {
                        packageString += ("import " + packagesFound.get(0) + ";\n");
                    }
                }
            }
        }
        return packageString;
    }
    
    /**
     * Helper method for converting an attribute to a String for exporting to Java source code
     * @param a
     *      the attribute to convert into a String
     * @return 
     *      the attribute as a String 
     */
    public String AttributeToString(AttributeDesign a) {
        String attribute = "\t" + a.getAccess() + " ";
        if (a.isAbstract()) {
            attribute += "abstract ";
        }
        if (a.isStatic()) {
            attribute += "static ";
        }
        attribute += (a.getType() + " " + a.getName());
        if (a.isMethod()) {
            attribute += "(";
            for (int i = 0; i < a.getParemeters().size(); i++) {
                attribute += (a.getParemeters().get(i).getText() + " arg" + (i + 1));
                if (i != a.getParemeters().size() - 1) {
                    attribute += ", ";
                }
            }
            attribute += ") {\n";
            if (!a.getType().equals("void") && !a.getType().isEmpty()) {
                if (a.getType().equals("Void")) {
                    attribute += ("\t\treturn null;\n");
                } else if (a.getType().equals("char")) {
                    attribute += ("\t\treturn 'd';\n");
                } else if (a.getType().equals("boolean")) {
                    attribute += ("\t\treturn false;\n");
                } else if (Character.isUpperCase(a.getType().charAt(0))) {
                    attribute += ("\t\treturn new " + a.getType() + "();\n");
                } else {
                    attribute += ("\t\treturn 0;\n");
                }
            }
            attribute += "\t}\n";
        } else {
            attribute += ";\n";
        }
        return attribute;
    }
    
    /**
     * Helper method for building the necessary files for NetBeans projects
     * @param projectName
     *      the name of the project to be built
     * @param filePath
     *      the file path of the project
     * @throws IOException 
     *      thrown if there's an error creating or finding a file
     */
    public void buildNetBeansProject(String projectName, String filePath) throws IOException {
        String build = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<project name=\"" + projectName + "\" default=\"default\" basedir=\".\">\n"
                + "\t<description>Builds, tests, and runs the project " + projectName + ".</description>\n"
                + "\t<import file=\"nbproject/build-impl.xml\"/>"
                + "</project>";
        File buildXMLFile = new File(filePath + "/build.xml" );
        PrintWriter pw1 = new PrintWriter(buildXMLFile.getPath());
        pw1.write(build);
        pw1.close();
        
        String manifest = "Manifest-Version: 1.0\nX-COMMENT: Main-Class will be added automatically by build";
        File manifestFile = new File(filePath + "/manifest.mf");
        PrintWriter pw2 = new PrintWriter(manifestFile.getPath());
        pw2.write(manifest);
        pw2.close();
        
        File JCDbuildImpl = new File("./nbproject/build-impl.xml");
        File buildImpl = new File(filePath + "/nbproject/build-impl.xml");
        PrintWriter pw3 = new PrintWriter(buildImpl.getPath());
        Scanner sc1 = new Scanner(JCDbuildImpl);
        String file1 = "";
        while (sc1.hasNextLine()) {
            file1 += (sc1.nextLine() + "\n");
        }
        pw3.write(file1);
        pw3.close();
        sc1.close();
        
        String genfileProp = "build.xml.data.CRC32=e5e7b627\n" +
                "build.xml.script.CRC32=080044f3\n" +
                "build.xml.stylesheet.CRC32=8064a381@1.79.1.48\n" + 
                "nbproject/build-impl.xml.data.CRC32=e5e7b627\n" +
                "nbproject/build-impl.xml.script.CRC32=78f3188e\n" +
                "nbproject/build-impl.xml.stylesheet.CRC32=05530350@1.79.1.48";
        File genfilePropFile = new File(filePath + "/nbproject/genfiles.properties");
        PrintWriter pw4 = new PrintWriter(genfilePropFile.getPath());
        pw4.write(genfileProp);
        pw4.close();
        
        File JCDprojProp = new File("./nbproject/project.properties");
        File projProp = new File(filePath + "/nbproject/project.properties");
        PrintWriter pw5 = new PrintWriter(projProp.getPath());
        Scanner sc2 = new Scanner(JCDprojProp);
        String file2 = "";
        while (sc2.hasNextLine()) {
            String temp = sc2.nextLine();
            if (!temp.contains("javax.json-1.0.4.jar") && !temp.contains("PropertiesManager.jar") && !temp.contains("SimpleAppFramework.jar") 
                    && !temp.contains("XMLUtilities.jar") && !temp.contains("JClassDesignerApp-data")) {
                file2 += (temp.replaceAll("JClassDesignerApp", projectName) + "\n");
            }
        }
        pw5.write(file2);
        pw5.close();
        sc2.close();
        
        File JCDproj = new File("./nbproject/project.xml");
        File proj = new File(filePath + "/nbproject/project.xml");
        PrintWriter pw6 = new PrintWriter(proj.getPath());
        Scanner sc3 = new Scanner(JCDproj);
        String file3 = "";
        while (sc3.hasNextLine()) {
            file3 += (sc3.nextLine().replaceAll("JClassDesignerApp", projectName) + "\n");
        }
        pw6.write(file3);
        pw6.close();
        sc3.close();
    }
}
