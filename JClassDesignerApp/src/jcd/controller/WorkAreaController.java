package jcd.controller;

import java.io.File;
import java.io.IOException;
import jcd.data.DataManager;
import jcd.data.ClassDesign;
import java.util.ArrayList;
import java.util.Stack;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import jcd.data.AttributeDesign;
import saf.AppTemplate;
import static saf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static saf.settings.AppPropertyType.SAVE_WORK_TITLE;
import static saf.settings.AppPropertyType.WORK_FILE_EXT;
import static saf.settings.AppPropertyType.WORK_FILE_EXT_DESC;
import static saf.settings.AppStartupConstants.PATH_WORK;
import saf.ui.AppMessageDialogSingleton;
import jcd.file.FileManager;

/**
 * @author Robert Riselvato
 */
public class WorkAreaController {
    
    // THIS IS THE APP
    AppTemplate app;
    
    // THIS IS THE DATA COMPONENT
    DataManager data;
    
    // THIS IS THE FILE MANAGER COMPONENT
    FileManager file;
    
    // THIS IS THE CURRENT FILE FOR EXPORTING
    private File currentExportFile;
    
    private Stack<DataManager> dataUndos;
    private Stack<DataManager> dataRedos;
    
    private double scaleFactor;
    
    /**
     * Constructs a new work area controller
     * Overloaded constructor
     * @param initApp 
     *      the application to control
     */
    public WorkAreaController(AppTemplate initApp) {
        app = initApp;
        data = (DataManager) app.getDataComponent();
        file = (FileManager) app.getFileComponent();
        dataUndos = new Stack<DataManager>();
        dataUndos.add(data.clone());
        dataRedos = new Stack<DataManager>();
        scaleFactor = 1;
    }
    
    /**
     * Adds a class to the list of designs
     * @param x
     *      the x position of the rectangle
     * @param y 
     *      the y position of the rectangle
     */
    public void handleAddClass(double x, double y) {
        data.addDesign(new ClassDesign(x - 100, y - 60, false));
    }
    
    /**
     * Add an interface to the list of designs
     * @param x
     *      the x position of the rectangle
     * @param y 
     *      the y position of the rectangle
     */
    public void handleAddInterface(double x, double y) {
        data.addDesign(new ClassDesign(x - 100, y - 60, true));
    }
    
    /**
     * Handles and manages the package name and class name of designs to prevent
     * duplicate classes in the same package
     * @param selectedDesign
     *      the design that is currently selected
     * @return 
     *      true if the selected design already exists in the package, false otherwise
     */
    public boolean handleClassAndPackageName(ClassDesign selectedDesign) {
        boolean exists = false;
        for (ClassDesign design : data.getDesigns()) {
            if (!design.equals(selectedDesign) && selectedDesign.getName().equals(design.getName())
                    && selectedDesign.getPackage().equals(design.getPackage()) && !"".equals(selectedDesign.getName())
                    /*&& !"".equals(selectedDesign.getPackage())*/) {
                AppMessageDialogSingleton.getSingleton().show("Duplicate Classes", "A class named \"" 
                        + selectedDesign.getName() + ".java\" already exists in the package \"" 
                        + selectedDesign.getPackage() + "\"");
                selectedDesign.setName("[!]" + selectedDesign.getName());
                if (!"".equals(selectedDesign.getPackage())) {
                    selectedDesign.setPackage("[!]" + selectedDesign.getPackage());
                }
                exists = true;
            }
        }
        return exists;
    }
    
    /**
     * Handles the request to export the work to Java source code
     */
    public void handleExportRequest() {
        try {
            FileChooser fc = new FileChooser();
            fc.setInitialDirectory(new File(PATH_WORK));
            fc.setTitle("Export your work to Java source code");
            fc.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("jClass Designer Java Files", "java"));

            File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
            if (selectedFile != null) {
		file.exportData(data, selectedFile.getPath());
                currentExportFile = selectedFile; 
	    }
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Exporting Completed", "File Exporting to Java Source Code Completed.");
        } catch (IOException ioe) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show("Export Work Error", "An error occurred exporting work.");
        }
    }
    
    public void handleAddVariable(ClassDesign selectedDesign) {
        selectedDesign.addVariable(new AttributeDesign(false));
        selectedDesign.reloadVariablesVBox();
    }
    
    public void handleRemoveVariable(ClassDesign selectedDesign, AttributeDesign variable) {
        if (!selectedDesign.getVariables().isEmpty()) {
            selectedDesign.getVariables().remove(variable);
            selectedDesign.reloadVariablesVBox();
        }
    }
    
    public void handleAddMethod(ClassDesign selectedDesign) {
        selectedDesign.addMethod(new AttributeDesign(true));
        selectedDesign.reloadMethodsVBox();
    }
    
    public void handleRemoveMethod(ClassDesign selectedDesign, AttributeDesign method) {
        if (!selectedDesign.getMethods().isEmpty()) {
            selectedDesign.getMethods().remove(method);
            selectedDesign.reloadMethodsVBox();
        }
    }
    
    public void enqueueUndo() {
        DataManager clone = data.clone();
        dataUndos.add(data.clone());
        dataRedos.clear();
    }
    
    public void handleUndo() {
        dataRedos.add(dataUndos.pop());
        data.updateData(dataUndos.peek());
    }
    
    public int getUndoSize() {
        return dataUndos.size();
    }
    
    public void resetUndo() {
        enqueueUndo();
    }
    
    public void handleRedo() {
        dataUndos.add(dataRedos.pop());
        data.updateData(dataUndos.peek());
    }
    
    public int getRedoSize() {
        return dataRedos.size();
    }
    
    public boolean handleZoomIn(Pane workspace) {
        double scale = (scaleFactor + 0.1) / scaleFactor;
        workspace.getTransforms().add(new Scale(scale, scale));
        scaleFactor = scaleFactor + 0.1;
        workspace.setPrefSize(4000 * scaleFactor, 2000 * scaleFactor);
        return scaleFactor > 2.99 && scaleFactor < 3.01;
    }
    
    public boolean handleZoomOut(Pane workspace) {
        workspace.getTransforms().remove(workspace.getTransforms().size() - 1);
        scaleFactor = scaleFactor - 0.1;
        workspace.setPrefSize(4000 * scaleFactor, 2000 * scaleFactor);
        return scaleFactor > 0.29 && scaleFactor < 0.31;
    }
    
    public void handleSnapAddClass(double x, double y) {
        double snapX;
        double snapY;
        if (((int) x) % 20 != 0) {
            if (((int) x) % 20 < 10) {
                snapX = (double) (((int) x) - (((int) x) % 20));
            } else {
                snapX = (double) (((int) x) - (((int) x) % 20) + 20);
            }
        } else {
            snapX = (double) ((int) x);
        }
        if (((int) y) % 20 != 0) {
            if (((int) y) % 20 < 10) {
                snapY = (double) (((int) y) - (((int) y) % 20));
            } else {
                snapY = (double) (((int) y) - (((int) y) % 20) + 20);
            }
        } else {
            snapY = (double) ((int) y);
        }
        data.addDesign(new ClassDesign(snapX - 100, snapY - 60, false));
    }
    
    public void handleSnapAddInterface(double x, double y) {
        double snapX;
        double snapY;
        if (((int) x) % 20 != 0) {
            if (((int) x) % 20 < 10) {
                snapX = (double) (((int) x) - (((int) x) % 20));
            } else {
                snapX = (double) (((int) x) - (((int) x) % 20) + 20);
            }
        } else {
            snapX = (double) ((int) x);
        }
        if (((int) y) % 20 != 0) {
            if (((int) y) % 20 < 10) {
                snapY = (double) (((int) y) - (((int) y) % 20));
            } else {
                snapY = (double) (((int) y) - (((int) y) % 20) + 20);
            }
        } else {
            snapY = (double) ((int) y);
        }
        data.addDesign(new ClassDesign(snapX - 100, snapY - 60, true));
    }
    
    public void handleSnapMove(ClassDesign design, double x, double y) {
        double snapX; double snapY;
        int intX = (int) (x - design.getWidth() / 2);
        int intY = (int) (y - design.getHeight() / 2);
        if (intX % 20 != 0) {
            if (intX % 20 < 10) {
                snapX = (double) (intX - (intX % 20));
            } else {
                snapX = (double) (intX - (intX % 20) + 20);
            }
        } else {
            snapX = (double) intX;
        }
        if (intY % 20 != 0) {
            if (intY % 20 < 10) {
                snapY = (double) (intY - (intY % 20));
            } else {
                snapY = (double) (intY - (intY % 20) + 20);
            }
        } else {
            snapY = (double) intY;
        }
        design.setXPosition(snapX);
        design.setYPosition(snapY);
        design.relocate(snapX, snapY);
    }
    
    public void handleSnapResize(ClassDesign design, double x, double y) {
        double snapWidth = Math.max(design.prefWidth(-1), x - design.getXPosition());
        double snapHeight = Math.max(design.prefHeight(-1), y - design.getYPosition());
        int intX = (int) snapWidth; int intY = (int) snapHeight;
        if (intX % 20 != 0) {
            snapWidth = (double) (intX - (intX % 20) + 22);
        } else {
            snapWidth = (double) intX;
        }
        if (intY % 20 != 0) {
            snapHeight = (double) (intY - (intY % 20) + 22);
        } else {
            snapHeight = (double) intY + 2;
        }
        
        design.setMinWidth(snapWidth);
        design.setMinHeight(snapHeight);
    }
    
    public void handleSnapshot(Pane workArea) {
        FileChooser fc = new FileChooser();
	fc.setInitialDirectory(new File(PATH_WORK));
	fc.setTitle("Choose where to save snapshot");
	fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG", "png"));
        
        File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
	if (selectedFile != null) {
            WritableImage snapshotImage = workArea.snapshot(new SnapshotParameters(), null);
            try {
                selectedFile = new File(selectedFile.getPath() + ".png");
                ImageIO.write(SwingFXUtils.fromFXImage(snapshotImage, null), "png", selectedFile);
                AppMessageDialogSingleton.getSingleton().show("Success", "Picture saved to " + selectedFile.getAbsolutePath());
            } catch (IOException fnfe) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Snapshot Error", "An error occurred while snapshoting.");
            }
        }
    }
}
