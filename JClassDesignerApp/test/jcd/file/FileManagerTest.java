package jcd.file;

import com.sun.javafx.application.PlatformImpl;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.scene.control.TextField;
import javax.json.JsonArray;
import javax.json.JsonObject;
import jcd.data.AttributeDesign;
import jcd.data.ClassDesign;
import jcd.data.DataManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import saf.components.AppDataComponent;

/**
 *
 * @author Rob
 */
public class FileManagerTest {
    
    DataManager data1;
    DataManager data2;
    FileManager file;
    
    public FileManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        PlatformImpl.startup(() -> {});
        file = new FileManager();
        data1 = new DataManager();
        try {
            file.loadData(data1, "./work/DesignSaveTest.json");
            System.out.println("Load Successful");
        } catch (IOException ioe) {
            System.out.println("Load Failed");
        }
        data2 = new DataManager();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testThreadExampleSaveLoad() {
        try {
            file.saveData(data1, "./work/DesignSaveTest.json");
            System.out.print("Save Successful");
        } catch (IOException ioe) { 
            System.out.print("Save Failed"); 
        }
        
        try {
            file.loadData(data2, "./work/DesignSaveTest.json");
            System.out.println("Load Successful");
        } catch (IOException ioe) {
            System.out.println("Load Failed");
        }
        
        ClassDesign threadExample1 = data1.getDesigns().get(1);
        ClassDesign threadExample2 = data2.getDesigns().get(1);
        AttributeDesign startText1 = threadExample1.getVariables().get(0);
        AttributeDesign startText2 = threadExample2.getVariables().get(0);
        AttributeDesign firstMethod1 = threadExample1.getMethods().get(0);
        AttributeDesign firstMethod2 = threadExample2.getMethods().get(0);
        AttributeDesign secondMethod1 = threadExample1.getMethods().get(4);
        AttributeDesign secondMethod2 = threadExample2.getMethods().get(4);
        
        assertEquals(threadExample1.getXPosition(), threadExample2.getXPosition(), 0);
        assertEquals(threadExample1.getYPosition(), threadExample2.getYPosition(), 0);
        assertEquals(startText1.getName(), startText2.getName());
        assertEquals(startText1.getType(), startText2.getType());
        assertEquals(firstMethod1.getName(), firstMethod2.getName());
        assertEquals(firstMethod1.getParemeters().get(0).getText(), firstMethod2.getParemeters().get(0).getText());
        assertEquals(secondMethod1.getName(), secondMethod2.getName());
        assertEquals(secondMethod1.getParemeters().get(0).getText(), secondMethod2.getParemeters().get(0).getText());
    }
    
    @Test
    public void testAbstractClassSaveLoad() {
        ClassDesign application1 = data1.getDesigns().get(0);
        AttributeDesign stylesheetCaspian = new AttributeDesign(false);
        stylesheetCaspian.setName("STYLESHEET_CASPIAN"); stylesheetCaspian.setIsStatic(true); stylesheetCaspian.setType("String"); application1.addVariable(stylesheetCaspian);
        AttributeDesign setUserAgentStylesheet = new AttributeDesign(true);
        setUserAgentStylesheet.setName("setUserAgentStylesheet"); setUserAgentStylesheet.setIsStatic(true); setUserAgentStylesheet.setType("void"); setUserAgentStylesheet.addParameter(new TextField("String")); application1.addMethod(setUserAgentStylesheet);
        File saveLoadTest1 = new File("./work/SaveLoadTest1.json");
        
        try {
            file.saveData(data1, "./work/SaveLoadTest1.json");
            System.out.print("Save Successful");
        } catch (IOException ioe) { 
            System.out.print("Save Failed"); 
        }
        
        try {
            file.loadData(data2, "./work/SaveLoadTest1.json");
            System.out.println("Load Successful");
        } catch (IOException ioe) {
            System.out.println("Load Failed");
        }
        
        ClassDesign application2 = data2.getDesigns().get(0);
        AttributeDesign variable1 = application1.getVariables().get(0);
        AttributeDesign variable2 = application2.getVariables().get(0);
        AttributeDesign firstMethod1 = application1.getMethods().get(0);
        AttributeDesign firstMethod2 = application2.getMethods().get(0);
        AttributeDesign secondMethod1 = application1.getMethods().get(1);
        AttributeDesign secondMethod2 = application2.getMethods().get(1);
        
        assertEquals(application1.getXPosition(), application2.getXPosition(), 0);
        assertEquals(application1.getYPosition(), application2.getYPosition(), 0);
        assertEquals(variable1.getName(), variable2.getName());
        assertEquals(variable1.getType(), variable2.getType());
        assertEquals(firstMethod1.getName(), firstMethod2.getName());
        assertEquals(firstMethod1.getParemeters().get(0).getText(), firstMethod2.getParemeters().get(0).getText());
        assertEquals(secondMethod1.getName(), secondMethod2.getName());
        assertEquals(secondMethod1.getParemeters().get(0).getText(), secondMethod2.getParemeters().get(0).getText());
    }
    
    @Test
    public void testInterfaceSaveLoad() {
        ClassDesign comparable1 = new ClassDesign(0, 500, true);
        comparable1.setName("Comparable"); comparable1.setPackage("java.lang");
        AttributeDesign compareTo = new AttributeDesign(true);
        compareTo.setName("compareTo"); compareTo.setType("int"); compareTo.addParameter(new TextField("T")); comparable1.addMethod(compareTo);
        data1.addDesign(comparable1);
        
        try {
            file.saveData(data1, "./work/SaveLoadTest2.json");
            System.out.print("Save Successful");
        } catch (IOException ioe) { 
            System.out.print("Save Failed"); 
        }
        
        try {
            file.loadData(data2, "./work/SaveLoadTest2.json");
            System.out.println("Load Successful");
        } catch (IOException ioe) {
            System.out.println("Load Failed");
        }
        
        ClassDesign counterTask1 = data1.getDesigns().get(3);
        ClassDesign counterTask2 = data2.getDesigns().get(3);
        ClassDesign eventHandler1 = data1.getDesigns().get(5);
        ClassDesign eventHandler2 = data2.getDesigns().get(5);
        ClassDesign comparable2 = data2.getDesigns().get(16);
        AttributeDesign variable1 = counterTask1.getVariables().get(1);
        AttributeDesign variable2 = counterTask2.getVariables().get(1);
        AttributeDesign firstMethod1 = eventHandler1.getMethods().get(0);
        AttributeDesign firstMethod2 = eventHandler2.getMethods().get(0);
        AttributeDesign secondMethod1 = comparable1.getMethods().get(0);
        AttributeDesign secondMethod2 = comparable2.getMethods().get(0);
        
        assertEquals(comparable1.getXPosition(), comparable2.getXPosition(), 0);
        assertEquals(comparable1.getYPosition(), comparable2.getYPosition(), 0);
        assertEquals(variable1.getName(), variable2.getName());
        assertEquals(variable1.getType(), variable2.getType());
        assertEquals(firstMethod1.getName(), firstMethod2.getName());
        assertEquals(firstMethod1.getParemeters().get(0).getText(), firstMethod2.getParemeters().get(0).getText());
        assertEquals(secondMethod1.getName(), secondMethod2.getName());
        assertEquals(secondMethod1.getParemeters().get(0).getText(), secondMethod2.getParemeters().get(0).getText());
    }
}
