import com.sun.javafx.application.PlatformImpl;
import java.io.IOException;
import javafx.scene.control.TextField;
import jcd.data.*;
import jcd.file.FileManager;

/**
 *
 * @author Rob
 */
public class TestLoad {
    public static void main(String[] args) {
        
        PlatformImpl.startup(() -> {});
        
        DataManager data = new DataManager();
        FileManager file = new FileManager();
        
        try {
            file.loadData(data, "./work/DesignSaveTest.json");
            System.out.println("Load Successful");
        } catch (IOException ioe) {
            System.out.println("Load Failed");
        }
        
        if (!data.getDesigns().isEmpty()) {
            for (ClassDesign design : data.getDesigns()) {
                System.out.println(design.getName());
                if (!design.getVariables().isEmpty()) {
                    for (AttributeDesign variable : design.getVariables()) {
                        System.out.println("\t" + variable.getName() + " : " + variable.getType());
                    }
                }
                if (!design.getMethods().isEmpty()) {
                    for (AttributeDesign method : design.getMethods()) {
                        System.out.println("\t" + method.getAccess() + " " + method.getName());
                        if (!method.getParemeters().isEmpty()) {
                            for (int i = 0; i < method.getParemeters().size(); i++) {
                                System.out.println("\t\targ" + (i + 1) + " : " + method.getParemeters().get(i).getText());
                            }
                        }
                    }
                }
                System.out.println("");
            }
        }
    }
}
