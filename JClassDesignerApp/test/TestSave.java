import com.sun.javafx.application.PlatformImpl;
import java.io.File;
import java.io.IOException;
import javafx.scene.control.TextField;
import jcd.data.*;
import jcd.file.*;

/**
 *
 * @author Rob
 */
public class TestSave {
    public static void main(String[] args) {
        
        PlatformImpl.startup(() -> {});
        
        DataManager data = new DataManager();
        FileManager file = new FileManager();
        
        // THE APPLICATION CLASS
        ClassDesign application = new ClassDesign(0, 0 , false);
        application.setName("Application");
        application.setPackage("javafx.application");
        application.setIsAbstract(true);
        AttributeDesign start = new AttributeDesign(true);
        start.setName("start"); start.setIsAbstract(true); start.addParameter(new TextField("Stage")); start.setType("void");
        application.addMethod(start);
        data.addDesign(application);
        
        // THE THREAD EXAMPLE CLASS
        ClassDesign threadExample = new ClassDesign(100, 100, false);
        threadExample.setName("ThreadExample");
        threadExample.addParent(application); 
        AttributeDesign startText = new AttributeDesign(false);
        startText.setType("String"); startText.setName("START_TEXT"); startText.setIsStatic(true); threadExample.addVariable(startText);
        AttributeDesign pauseText = new AttributeDesign(false);
        pauseText.setType("String"); pauseText.setName("PAUSE_TEXT"); pauseText.setIsStatic(true); threadExample.addVariable(pauseText);
        AttributeDesign window = new AttributeDesign(false);
        window.setType("Stage"); window.setName("window"); window.setAccess("private"); threadExample.addVariable(window);
        AttributeDesign appPane = new AttributeDesign(false);
        appPane.setType("BorderPane"); appPane.setName("appPane"); appPane.setAccess("private"); threadExample.addVariable(appPane);
        AttributeDesign topPane = new AttributeDesign(false);
        topPane.setType("FlowPane"); topPane.setName("topPane"); topPane.setAccess("private"); threadExample.addVariable(topPane);
        AttributeDesign startButton = new AttributeDesign(false);
        startButton.setType("Button"); startButton.setName("startButton"); startButton.setAccess("private"); threadExample.addVariable(startButton);
        AttributeDesign pauseButton = new AttributeDesign(false);
        pauseButton.setType("Button"); pauseButton.setName("pauseButton"); pauseButton.setAccess("private"); threadExample.addVariable(pauseButton);
        AttributeDesign scrollPane = new AttributeDesign(false);
        scrollPane.setType("ScrollPane"); scrollPane.setName("scrollPane"); scrollPane.setAccess("private"); threadExample.addVariable(scrollPane);
        AttributeDesign textArea = new AttributeDesign(false);
        textArea.setType("TextArea"); textArea.setName("textArea"); textArea.setAccess("private"); threadExample.addVariable(textArea);
        AttributeDesign dataThread = new AttributeDesign(false);
        dataThread.setType("Thread"); dataThread.setName("dataThread"); dataThread.setAccess("private"); threadExample.addVariable(dataThread);
        AttributeDesign dataTask = new AttributeDesign(false);
        dataTask.setType("Task"); dataTask.setName("dataTask"); dataTask.setAccess("private"); threadExample.addVariable(dataTask);
        AttributeDesign counterThread = new AttributeDesign(false);
        counterThread.setType("Thread"); counterThread.setName("counterThread"); counterThread.setAccess("private"); threadExample.addVariable(counterThread);
        AttributeDesign counterTask = new AttributeDesign(false);
        counterTask.setType("Task"); counterTask.setName("counterTask"); counterTask.setAccess("private"); threadExample.addVariable(counterTask);
        AttributeDesign work = new AttributeDesign(false);
        work.setType("boolean"); work.setName("work"); work.setAccess("private"); threadExample.addVariable(work);
        AttributeDesign start1 = new AttributeDesign(true);
        start1.setName("start"); start1.setType("void"); start1.addParameter(new TextField("Stage")); threadExample.addMethod(start1);
        AttributeDesign startWork = new AttributeDesign(true);
        startWork.setName("startWork"); startWork.setType("void"); threadExample.addMethod(startWork);
        AttributeDesign pauseWork = new AttributeDesign(true);
        pauseWork.setName("pauseWork"); pauseWork.setType("void"); threadExample.addMethod(pauseWork);
        AttributeDesign doWork = new AttributeDesign(true);
        doWork.setName("doWork"); doWork.setType("boolean"); threadExample.addMethod(doWork);
        AttributeDesign appendText = new AttributeDesign(true);
        appendText.setName("appendText"); appendText.setType("void"); appendText.addParameter(new TextField("String")); threadExample.addMethod(appendText);
        AttributeDesign sleep = new AttributeDesign(true);
        sleep.setName("sleep"); sleep.setType("void"); sleep.addParameter(new TextField("int")); threadExample.addMethod(sleep);
        AttributeDesign initLayout = new AttributeDesign(true);
        initLayout.setName("initLayout"); initLayout.setType("void"); initLayout.setAccess("private"); threadExample.addMethod(initLayout);
        AttributeDesign initHandlers = new AttributeDesign(true);
        initHandlers.setName("initHandlers"); initHandlers.setType("void"); initHandlers.setAccess("private"); threadExample.addMethod(initHandlers);
        AttributeDesign initWindow = new AttributeDesign(true);
        initWindow.setName("initWindow"); initWindow.setType("void"); initWindow.setAccess("private"); initWindow.addParameter(new TextField("Stage")); threadExample.addMethod(initWindow);
        AttributeDesign initThreads = new AttributeDesign(true);
        initThreads.setName("initThreads"); initThreads.setType("void"); initThreads.setAccess("private"); threadExample.addMethod(initThreads);
        AttributeDesign main = new AttributeDesign(true);
        main.setName("main"); main.setType("void"); main.setIsStatic(true); main.addParameter(new TextField("String[]")); threadExample.addMethod(main);
        data.addDesign(threadExample);
        
        // THE TASK CLASS
        ClassDesign task = new ClassDesign(200, 200, false);
        task.setName("Task");
        task.setPackage("javafx.concurrent");
        task.setIsAbstract(true);
        data.addDesign(task);
        
        // THE COUNTER TASK CLASS
        ClassDesign counterTask1 = new ClassDesign(300, 300, false);
        counterTask1.setName("CounterTask");
        counterTask1.addParent(task);
        AttributeDesign app = new AttributeDesign(false);
        app.setName("app"); app.setType("ThreadExample"); app.setAccess("private"); counterTask1.addVariable(app);
        AttributeDesign counter = new AttributeDesign(false);
        counter.setName("counter"); counter.setType("int"); counter.setAccess("private"); counterTask1.addVariable(counter);
        AttributeDesign counterTask2 = new AttributeDesign(true);
        counterTask2.setName("CounterTask"); counterTask2.addParameter(new TextField("ThreadExample")); counterTask1.addMethod(counterTask2);
        AttributeDesign call = new AttributeDesign(true);
        call.setName("call"); call.setType("Void"); call.setAccess("protected"); counterTask1.addMethod(call);
        data.addDesign(counterTask1);
        
        // THE DATE TASK CLASS
        ClassDesign dateTask = new ClassDesign(400, 400, false);
        dateTask.setName("DateTask");
        dateTask.addParent(task);
        dateTask.addVariable(app);
        AttributeDesign now = new AttributeDesign(false);
        now.setName("now"); now.setType("Date"); now.setAccess("private"); dateTask.addVariable(now);
        AttributeDesign dateTask1 = new AttributeDesign(true);
        dateTask1.setName("DateTask"); dateTask1.addParameter(new TextField("ThreadExample")); dateTask.addMethod(dateTask1);
        dateTask.addMethod(call);
        data.addDesign(dateTask);
        
        // THE EVENT HANDLER INTERFACE 
        ClassDesign eventHandler = new ClassDesign(500, 500, true);
        eventHandler.setName("EventHandler");
        eventHandler.setPackage("javafx.event");
        AttributeDesign handle = new AttributeDesign(true);
        handle.setName("handle"); handle.setType("void"); handle.addParameter(new TextField("Event")); eventHandler.addMethod(handle);
        data.addDesign(eventHandler);
        
        // THE PAUSE HANDLER CLASS
        ClassDesign pauseHandler = new ClassDesign(600, 600, false);
        pauseHandler.setName("PauseHandler");
        pauseHandler.addParent(eventHandler);
        pauseHandler.addVariable(app);
        AttributeDesign pauseHandler1 = new AttributeDesign(true);
        pauseHandler1.setName("PauseHandler"); pauseHandler1.addParameter(new TextField("ThreadExample")); pauseHandler.addMethod(pauseHandler1);
        pauseHandler.addMethod(handle);
        data.addDesign(pauseHandler);
        
        // THE START HANDLER CLASS
        ClassDesign startHandler = new ClassDesign(700, 700, false);
        startHandler.setName("StartHandler");
        startHandler.addParent(eventHandler);
        startHandler.addVariable(app);
        AttributeDesign startHandler1 = new AttributeDesign(true);
        startHandler1.setName("StartHandler"); startHandler1.addParameter(new TextField("ThreadExample")); startHandler.addMethod(startHandler1);
        startHandler.addMethod(handle);
        data.addDesign(startHandler);
        
        // THE DATE CLASS
        ClassDesign date = new ClassDesign(800, 800, false);
        date.setName("Date"); date.setPackage("java.util");
        data.addDesign(date);
        
        // THE STAGE CLASS
        ClassDesign stage = new ClassDesign(900, 900, false);
        stage.setName("Stage"); stage.setPackage("javafx.stage");
        data.addDesign(stage);
        
        // THE BORDER PANE CLASS
        ClassDesign borderPane = new ClassDesign(1000, 1000, false);
        borderPane.setName("BorderPane"); borderPane.setPackage("javafx.scene.layout");
        data.addDesign(borderPane);
        
        // THE FLOW PANE CLASS
        ClassDesign flowPane = new ClassDesign(1100, 1100, false);
        flowPane.setName("FlowPane"); flowPane.setPackage("javafx.scene.layout");
        data.addDesign(flowPane);
        
        // THE BUTTON CLASS
        ClassDesign button = new ClassDesign(1200, 1200, false);
        button.setName("Button"); button.setPackage("javafx.scene.control");
        data.addDesign(button);
        
        // THE SCROLL PANE CLASS
        ClassDesign scrollPane1 = new ClassDesign(1300, 1300, false);
        scrollPane1.setName("ScrollPane"); scrollPane1.setPackage("javafx.scene.control");
        data.addDesign(scrollPane1);
        
        // THE TEXT AREA CLASS
        ClassDesign textArea1 = new ClassDesign(1400, 1400, false);
        textArea1.setName("TextArea"); textArea1.setPackage("javafx.scene.control");
        data.addDesign(textArea1);
        
        // THE THREAD CLASS
        ClassDesign thread = new ClassDesign(1500, 1500, false);
        thread.setName("Thread"); thread.setPackage("java.lang");
        data.addDesign(thread);
        
        (new File("./work/")).mkdir();
        File saveFile = new File("./work/DesignSaveTest.json");
        try {
            file.saveData(data, "./work/DesignSaveTest.json");
            System.out.print("Save Successful");
        } catch (IOException ioe) { System.out.print("Save Failed"); }
    }
}
